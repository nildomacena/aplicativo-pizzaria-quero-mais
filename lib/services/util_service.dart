import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_page.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';

class UtilService {
  double latitude = -9.5763561;
  double longitude = -35.7543298;

  void snackBarErro({String titulo, String mensagem}) {
    Get.snackbar(
      titulo ?? 'Erro',
      mensagem ?? 'Erro durante a operação',
      snackPosition: SnackPosition.TOP,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
      duration: Duration(seconds: 5),
    );
  }

  void snackBar(
      {@required String titulo,
      @required String mensagem,
      SnackPosition snackPosition,
      String actionNome,
      Function action}) {
    Get.snackbar(
      titulo,
      mensagem,
      mainButton: actionNome == null || action == null
          ? null
          : TextButton(
              onPressed: action,
              child: Text(actionNome),
            ),
      snackPosition: snackPosition ?? SnackPosition.TOP,
      backgroundColor: Colors.white,
      //colorText: Colors.white,
      margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
      duration: Duration(seconds: 5),
    );
  }

  snackBarItemAdicionado(Item item) {
    utilService.snackBar(
        titulo: 'Adicionado',
        mensagem: 'O item ${item.nome} foi adicionado ao carrinho',
        actionNome: 'Ir para o Carrinho',
        action: () {
          Get.toNamed(Routes.CARRINHO);
        });
  }

  showAlert(String titulo, String mensagem,
      {Function action, String actionLabel}) {
    return Get.dialog(
      AlertDialog(
        title: Text(titulo),
        content: Container(
          child: Text(mensagem),
        ),
        actions: [
          TextButton(
              onPressed: () {
                Get.back();
              },
              child: Text('CANCELAR')),
          if (action != null)
            TextButton(onPressed: action, child: Text(actionLabel)),
        ],
      ),
    );
  }

  void showAlertCarregando([String mensagem]) {
    Get.dialog(
        AlertDialog(
            content: Container(
                height: 80,
                child: Column(children: <Widget>[
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                  ),
                  Text(
                    mensagem ?? 'Fazendo consulta...',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Get.theme.primaryColor),
                  )
                ]))),
        barrierDismissible: false);
  }

  fecharAlert() {
    if (Get.isDialogOpen) Get.back();
  }

  Future<String> locationToString(LocationData locationData) async {
    Address address;
    String enderecoLocation;
    final coordinates =
        new Coordinates(locationData.latitude, locationData.longitude);
    List<Address> addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    address = addresses.first;
    if (address.addressLine.indexOf(' - AL, 5') > 0)
      enderecoLocation = address.addressLine
          .substring(0, address.addressLine.indexOf(' - AL, 5'));
    return enderecoLocation;
  }

  Future<Address> locationToAddress(LocationData locationData) async {
    Address address;
    final coordinates =
        new Coordinates(locationData.latitude, locationData.longitude);
    List<Address> addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    address = addresses.first;
    return address;
  }

  Future<void> lauchUrl(String url) async {
    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }
}

UtilService utilService = UtilService();

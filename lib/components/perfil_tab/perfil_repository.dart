import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class PerfilRepository {
  final FirestoreProvider firestoreProvider = Get.find();
  final AuthProvider authProvider = Get.find();

  Future<void> loginComGoogle() {
    return authProvider.loginComGoogle();
  }

  Stream<Usuario> streamUsuario() {
    return authProvider.usuario$;
  }

  Stream<List<Endereco>> streamEnderecos() {
    return authProvider.enderecos$;
  }

  Future<Usuario> getCurrentUsuario() {
    return authProvider.getCurrentUsuario();
  }

  Stream<bool> streamPossuiPedidosPendentes() {
    return authProvider.streamPossuiPendentes();
  }

  Future<double> getCashbackUsuario() {
    return authProvider.getCashbackUsuario();
  }

  Future<void> logout() {
    return authProvider.logout();
  }
}

import 'package:app_pizzaria_quero_mais/components/perfil_tab/custom_wigets/nao_logado_page.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/perfil_tab/custom_wigets/item_lista.dart';
import 'package:app_pizzaria_quero_mais/components/perfil_tab/perfil_controller.dart';

class PerfilTab extends StatelessWidget {
  final PerfilController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    Widget streamPedidosPendentes() {
      return StreamBuilder<bool>(
          stream: controller.possuiPedidosPendentes$,
          builder: (context, AsyncSnapshot<bool> snapshot) {
            print('AsyncSnapshot<bool>: ${snapshot.data}');
            if (!snapshot.hasData) return Container();
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return Container();
              default:
                if (!snapshot.data) Container();
                return Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(7.5),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 8,
                    minHeight: 8,
                  ),
                  child: Text(
                    'Você possui pedidos pendentes',
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                    ),
                    textAlign: TextAlign.center,
                  ),
                );
            }
          });
    }

    return Container(
        /* height: Get.height,*/
        width: Get.width,
        child: GetX<PerfilController>(
          builder: (_) {
            if (controller.usuario == null) return NaoLogadoPage();
            /* return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        controller.loginComGoogle();
                      },
                      child: Text('LOGIN COM GOOGLE'))
                ],
              ); */
            return ListView(
              children: [
                GetX<PerfilController>(builder: (_) {
                  return Material(
                    elevation: 2,
                    child: InkWell(
                      onTap: () {
                        controller.editarDados();
                        /* print('Editar perfil');
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => EditarDadosPage())); */
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        width: MediaQuery.of(context).size.width,
                        child: Container(
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(bottom: 5),
                                    child: Text(
                                      "Nome Completo",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 19),
                                    ),
                                  ),
                                  AutoSizeText(
                                    controller.usuario.nome == null ||
                                            controller.usuario.nome == ''
                                        ? 'Clique aqui para definir seu nome'
                                        : controller.usuario.nome,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w300,
                                        fontSize: 17),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(bottom: 5, top: 10),
                                    child: AutoSizeText(
                                      "Telefone de contato",
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 19),
                                    ),
                                  ),
                                  AutoSizeText(
                                    controller.usuario.celular == null ||
                                            controller.usuario.celular == ''
                                        ? 'Clique para definir o nº do seu celular'
                                        : controller.usuario.celular,
                                    maxLines: 1,
                                    maxFontSize: 17,
                                    minFontSize: 15,
                                    style:
                                        TextStyle(fontWeight: FontWeight.w300),
                                  )
                                ],
                              ),
                              Expanded(
                                child: Container(),
                              ),
                              AutoSizeText(
                                'Editar',
                                maxLines: 1,
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w300),
                              ),
                              Icon(
                                Icons.arrow_forward_ios,
                                size: 20,
                                color: Colors.grey[400],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
                ItemLista(
                  icone: FontAwesome5Solid.book,
                  titulo: 'Pedidos',
                  badge: streamPedidosPendentes(),
                  subtitulo: 'Seu histórico de pedidos',
                  function: controller.goToPedidos,
                ),
                ItemLista(
                  icone: Icons.location_on,
                  titulo: 'Endereços',
                  subtitulo: 'Seus endereços cadastrados',
                  function: controller.goToEnderecos,
                ),
                ItemLista(
                  icone: Icons.exit_to_app,
                  titulo: 'Sair',
                  subtitulo: 'Desconectar sua conta',
                  function: controller.logout,
                ),
                if (controller.cashbackUsuario != null &&
                    controller.cashbackUsuario > 0)
                  Container(
                    width: Get.width,
                    padding: EdgeInsets.all(10),
                    child: AutoSizeText(
                      'Você tem R\$${controller.cashbackUsuario.toStringAsFixed(2)} de desconto na próxima compra',
                      maxLines: 1,
                      style: TextStyle(fontSize: 20),
                    ),
                  )
              ],
            );
          },
        ));
  }
}

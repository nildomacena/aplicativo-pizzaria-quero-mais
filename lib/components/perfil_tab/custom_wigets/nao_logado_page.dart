import 'package:app_pizzaria_quero_mais/components/perfil_tab/perfil_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:get/get.dart';

class NaoLogadoPage extends StatelessWidget {
  PerfilController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Text(
              'Para fazer pedidos, participar de sorteios e ter acesso a descontos, faça login com sua conta Google.\nÉ simples e rápido, basta clicar no botão abaixo',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
            ),
          ),
          SignInButton(
            Buttons.GoogleDark,
            text: 'FAÇA LOGIN COM GOOGLE',
            padding: EdgeInsets.only(left: 10, right: 10),
            onPressed: () {
              controller.loginComGoogle();
            },
          ),
          /* ElevatedButton(
              onPressed: () {
                controller.loginComGoogle();
              },
              child: Text(
                'LOGIN COM GOOGLE',
                style: TextStyle(fontSize: 16),
              )) */
        ],
      ),
    );
  }
}

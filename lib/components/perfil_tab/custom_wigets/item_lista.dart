import 'package:flutter/material.dart';

class ItemLista extends StatelessWidget {
  final IconData icone;
  final String titulo;
  final Function function;
  final Widget badge;
  final String subtitulo;

  ItemLista(
      {@required this.icone,
      @required this.titulo,
      @required this.subtitulo,
      this.function,
      this.badge});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: function,
      child: Stack(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.grey))),
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Icon(
                    icone,
                    size: 31,
                    color: Colors.grey,
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 3),
                        child: Text(
                          titulo,
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w400),
                        ),
                      ),
                      Text(
                        subtitulo ?? '',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          if (badge != null) Positioned(right: 5, top: 15, child: badge)
        ],
      ),
    );
  }
}

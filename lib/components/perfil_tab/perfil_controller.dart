import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_page.dart';
import 'package:app_pizzaria_quero_mais/components/perfil_tab/perfil_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class PerfilController extends GetxController {
  final PerfilRepository repository = Get.put(PerfilRepository());
  final Rx<Usuario> _usuario = Usuario().obs;
  final RxList<Endereco> _enderecos = RxList<Endereco>();
  final RxBool _possuiPedidosPendentes = RxBool(false);
  double cashbackUsuario = 0;

  bool get possuiPedidosPendentes => _possuiPedidosPendentes.value;
  Stream<bool> get possuiPedidosPendentes$ => _possuiPedidosPendentes.stream;

  Usuario get usuario => _usuario.value;
  set usuario(Usuario value) => this._usuario.value = value;

  PerfilController() {}

  @override
  onInit() {
    super.onInit();
    print('onInit perfil controller ');
    _enderecos.bindStream(repository.streamEnderecos());
    _usuario.bindStream(repository.authProvider.usuario$);
    _possuiPedidosPendentes
        .bindStream(repository.streamPossuiPedidosPendentes());
    _usuario.listen((usuario) {
      print('usuario: $usuario');
    });
    getCashbackUsuario();
  }

  getCashbackUsuario() async {
    cashbackUsuario = await repository.getCashbackUsuario();
    update();
  }

  editarDados() async {
    await Get.to(() => EditarDadosPage(),
        fullscreenDialog: true, arguments: {'usuario': usuario});
    usuario = await repository.getCurrentUsuario();
  }

  goToEnderecos() {
    Get.toNamed(Routes.ENDERECOS);
  }

  goToPedidos() {
    Get.toNamed(Routes.PEDIDOS);
  }

  loginComGoogle() async {
    try {
      utilService.showAlertCarregando('Fazendo login...');
      await repository.loginComGoogle();
      utilService.fecharAlert();
    } catch (e) {
      print('Erro: $e');
      utilService.fecharAlert();
      utilService.snackBarErro();
    }
  }

  logout() async {
    try {
      await repository.logout();
      usuario = null;
    } catch (e) {
      utilService.snackBarErro();
    }
  }
}

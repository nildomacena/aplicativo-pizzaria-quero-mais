import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/custom_widgets/card_categoria.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/custom_widgets/row_destaques.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/custom_widgets/botao_carrinho/botao_carrinho.dart';

class CardapioTab extends StatelessWidget {
  CardapioController controller = Get.find();
  CarrinhoController carrinhoController = Get.find();
  Widget columnCategorias() {
    double height = Get.height - 83;
    int numCategorias = controller.categorias.length;

    return SafeArea(
      child: Container(
        width: Get.width,
        height: height,
        //color: Colors.green,
        child: Stack(
          //Stack adicionado para exibir o container de modo teste. Retirar quando acabarem os testes
          children: [
            LayoutBuilder(
              builder: (context, constraints) {
                return GetBuilder<CarrinhoController>(builder: (_) {
                  bool carrinhoNaoVazio =
                      carrinhoController.entradas.isNotEmpty ||
                          carrinhoController.pizzas.isNotEmpty;
                  return Column(
                      children: controller.categorias
                          .map((c) => CardCategoria(
                              c,
                              ((constraints.maxHeight -
                                          (carrinhoNaoVazio ? 60 : 0)) /
                                      numCategorias) -
                                  10))
                          .toList());
                });
              },
            ),
            Positioned(
                top: 10,
                child: Container(
                  alignment: Alignment.center,
                  width: Get.width,
                  color: Colors.grey,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Feather.alert_triangle,
                        color: Colors.yellow,
                        size: 30,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text('VOCÊ ESTÁ EM MODO TESTE',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold)),
                      )
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        fit: StackFit.expand,
        children: [
          GetBuilder<CardapioController>(
            builder: (_) {
              if (controller.categorias == null)
                return Container(
                  height: Get.height,
                  width: Get.width,
                  color: Colors.red,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Image.asset(
                        'assets/splashscreen.png',
                        fit: BoxFit.cover,
                      ),
                      Positioned(
                        bottom: 100,
                        child: Column(
                          children: [
                            Container(
                              height: 50,
                              width: Get.width,
                              alignment: Alignment.center,
                              child: Container(
                                height: 40,
                                width: 40,
                                child: CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation<Color>(Colors.red),
                                  backgroundColor: Colors.white,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text(
                                'Carregando Cardápio',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              return columnCategorias();
              return ListView(
                children: [
                  if (controller.destaques.isNotEmpty) RowDestaque(controller),
                  columnCategorias(),
                  //Divider(),
                  /*  StaggeredGridView.countBuilder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      itemCount: controller.categorias.length,
                      staggeredTileBuilder: (int index) =>
                          StaggeredTile.count(2, 1),
                      itemBuilder: (BuildContext context, int index) {
                        return CardCategoria(controller.categorias[index]);
                      }), 
                  Container(
                    padding: EdgeInsets.all(35),
                  )*/
                ],
              );
            },
          ),
          Positioned(bottom: 0, child: BotaoCarrinho())
        ],
      ),
    );
  }
}

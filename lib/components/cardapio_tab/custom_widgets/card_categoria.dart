import 'package:auto_size_text/auto_size_text.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';

class CardCategoria extends StatelessWidget {
  final Categoria categoria;
  final height;
  final CardapioController controller = Get.find();
  CardCategoria(this.categoria, this.height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      margin: EdgeInsets.all(5),
      child: Material(
        elevation: 5,
        child: Container(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              ExtendedImage.network(
                categoria.imagemUrl,
                fit: BoxFit.cover,
              ),
              InkWell(
                splashColor: Colors.red,
                onTap: () {
                  controller.irParaCategoria(categoria);
                },
                child: Container(
                  color: Colors.black.withOpacity(.5),
                ),
              ),
              GestureDetector(
                onTap: () {
                  controller.irParaCategoria(categoria);
                  //irParaCategoria(context, categorias[index]);
                },
                child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(5),
                    child: AutoSizeText(
                      categoria.nome,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 23,
                          color: Colors.white,
                          shadows: [
                            Shadow(
                              offset: Offset(2, 2),
                              blurRadius: 3.0,
                              color: Colors.black,
                            ),
                            Shadow(
                              offset: Offset(2, 2),
                              blurRadius: 3.0,
                              color: Colors.black,
                            )
                          ]),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}

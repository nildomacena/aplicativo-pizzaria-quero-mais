import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BottomSheetSabores extends StatelessWidget {
  final CardapioController controller;

  BottomSheetSabores(this.controller);

  @override
  Widget build(BuildContext context) {
    //return Container();
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
      //height: 150,
      width: Get.width,
      color: Colors.white,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Text(
              'Escolha quantos sabores você vai querer nessa pizza?',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
            ),
          ),
          Divider(),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Get.theme
                            .primaryColor /* !_.escolheu
                              ? Colors.grey
                              : !_.doisSabores
                                  ? Get.theme.primaryColor
                                  : Colors.grey */
                        ),
                    onPressed: () {
                      Get.back(result: false);
                    },
                    child: Text('UM SABOR'),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(5),
                ),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Get.theme
                            .primaryColor /* !_.escolheu
                              ? Colors.grey
                              : _.doisSabores
                                  ? Get.theme.primaryColor
                                  : Colors.grey */
                        ),
                    onPressed: () {
                      Get.back(result: true);
                    },
                    child: Text('DOIS SABORES'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    return GetBuilder<CardapioController>(builder: (_) {
      return Container(
        padding: EdgeInsets.only(left: 20, right: 20, bottom: 30),
        //height: 150,
        width: Get.width,
        color: Colors.white,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: [
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                'Escolha quantos sabores você vai querer nessa pizza?',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
              ),
            ),
            Divider(),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Get.theme
                              .primaryColor /* !_.escolheu
                              ? Colors.grey
                              : !_.doisSabores
                                  ? Get.theme.primaryColor
                                  : Colors.grey */
                          ),
                      onPressed: () {
                        controller.escolherQuantidade(false);
                      },
                      child: Text('UM SABOR'),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                  ),
                  Expanded(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: Get.theme
                              .primaryColor /* !_.escolheu
                              ? Colors.grey
                              : _.doisSabores
                                  ? Get.theme.primaryColor
                                  : Colors.grey */
                          ),
                      onPressed: () {
                        controller.escolherQuantidade(true);
                      },
                      child: Text('DOIS SABORES'),
                    ),
                  ),
                ],
              ),
            ),
            if (controller.escolheu)
              Container(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.red[900]),
                  child: Text(
                      _.doisSabores ? 'ESCOLHER SABORES' : 'ESCOLHER SABOR'),
                  onPressed: () {},
                ),
              )
          ],
        ),
      );
    });
  }
}

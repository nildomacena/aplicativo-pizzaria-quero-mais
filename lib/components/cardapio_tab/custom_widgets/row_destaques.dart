import 'package:auto_size_text/auto_size_text.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';

class RowDestaque extends StatelessWidget {
  final CardapioController controller;
  RowDestaque(this.controller);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CardapioController>(builder: (_) {
      if (controller.destaques == null)
        return Center(child: CircularProgressIndicator());
      return Container(
        height: 138,
        margin: EdgeInsets.only(top: 20),
        child: ListView(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          children: controller.destaques
              .map((Item destaque) => Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    width: 200,
                    child: Card(
                      elevation: 4,
                      child: InkWell(
                        splashColor: Colors.red[200],
                        onTap: () {
                          print('click destaque');
                          controller.goToItem(destaque);
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 130,
                              width: MediaQuery.of(context).size.width,
                              child: Stack(
                                fit: StackFit.expand,
                                children: <Widget>[
                                  Hero(
                                    tag: destaque.id,
                                    child: ExtendedImage.network(
                                      destaque.imagemUrl,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 30,
                                    left: 10,
                                    child: Container(
                                      width: 120,
                                      padding: EdgeInsets.only(right: 20),
                                      child: AutoSizeText(
                                        '${destaque.nome}',
                                        maxLines: 2,
                                        minFontSize: 18,
                                        maxFontSize: 24,
                                        style: TextStyle(
                                            //fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                            shadows: [
                                              Shadow(
                                                offset: Offset(2, 2),
                                                blurRadius: 3.0,
                                                color: Colors.black,
                                              ),
                                              Shadow(
                                                offset: Offset(2, 2),
                                                blurRadius: 3.0,
                                                color: Colors.black,
                                              )
                                            ]),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            /*   Container(
                              margin: EdgeInsets.only(left: 10, top: 10),
                              padding: EdgeInsets.all(5),
                              child: AutoSizeText(
                                destaque.descricao,
                                maxLines: 5,
                                minFontSize: 16,
                                maxFontSize: 18,
                                textAlign: TextAlign.left,
                                style: TextStyle(fontWeight: FontWeight.w300),
                              ),
                            ) */
                          ],
                        ),
                      ),
                    ),
                  ))
              .toList(),
        ),
      );
    });
  }
}

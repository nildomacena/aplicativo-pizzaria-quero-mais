import 'package:app_pizzaria_quero_mais/components/cardapio_tab/custom_widgets/bottomsheet_sabores.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/data/provider/pizzas_provider.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';

class CardapioController extends GetxController {
  final CardapioRepository repository = Get.find();
  final PizzasProvider pizzasProvider = Get.find();
  final CarrinhoController carrinhoController = Get.find();

  List<Categoria> categorias;
  List<Item> destaques;
  bool doisSabores = false;
  bool escolheu = false;
  bool modoTeste = false;

  @override
  void onInit() {
    initFutures();
    super.onInit();
  }

  void initFutures() async {
    try {
      repository.checkModoTeste().then((modoTeste) {
        this.modoTeste = modoTeste;
      });
      categorias = await repository.getCategorias();
      destaques = await repository.getDestaques();
      update();
    } catch (e) {
      print('erro durante a busca: $e');
    }
  }

  goToItem(Item item) {
    Get.toNamed(Routes.ITEM, arguments: {'item': item});
  }

  toggleDoisSabores(bool value) {
    doisSabores = value;
    escolheu = true;
    update();
  }

  escolherQuantidade(bool value) {
    Get.back(result: value);
  }

  irParaCategoria(Categoria categoria) async {
    // print('Categoria: $categoria');
    if (pizzasProvider.doisSabores) {
      Get.toNamed(Routes.CATEGORIA, arguments: {'categoria': categoria});
      return;
    }

    var result = await Get.bottomSheet(BottomSheetSabores(this));
    if (result == null) {
      doisSabores = false;
      escolheu = false;
      return;
    }
    pizzasProvider.doisSabores = result;
    Get.toNamed(Routes.CATEGORIA, arguments: {'categoria': categoria});
  }
}

import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class CardapioRepository {
  final FirestoreProvider firestoreProvider = Get.find();

  CardapioRepository();

  Future<List<Categoria>> getCategorias() {
    return firestoreProvider.getCategorias();
  }

  Future<List<Item>> getDestaques() {
    return firestoreProvider.getDestaques();
  }

  Future<bool> checkModoTeste() {
    return firestoreProvider.checkModoTeste();
  }
}

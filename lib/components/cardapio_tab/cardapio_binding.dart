import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_repository.dart';

class CardapioBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CardapioController>(
      () => CardapioController(),
    );
  }
}

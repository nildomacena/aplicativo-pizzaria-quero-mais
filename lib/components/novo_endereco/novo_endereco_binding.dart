import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_controller.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_repository.dart';

class NovoEnderecoBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NovoEnderecoRepository>(() => NovoEnderecoRepository());
    Get.lazyPut<NovoEnderecoController>(() => NovoEnderecoController());
  }
}

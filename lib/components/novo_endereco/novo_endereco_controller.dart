import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:location/location.dart';

class NovoEnderecoController extends GetxController {
  final NovoEnderecoRepository repository = Get.find();
  LocationData locationData; // Localização trazida da pagina de enderecos
  LocationData
      locationDataFinal; // Localizacao escolhida pelo usuário manualmente
  Marker marker;
  List<Marker> markers = [];
  Completer<GoogleMapController> completerGoogleMap = Completer();
  GoogleMapController _mapController;
  CameraPosition cameraPosition;
  Address enderecoGeocode;
  bool semNumero = false;
  bool salvando = false;
  bool ocupado = false;
  bool editarLocalizacao = false;

  Location location = Location();
  Address address;
  String enderecoLocation = '';

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nomeController = TextEditingController();
  TextEditingController logradouroController = TextEditingController();
  TextEditingController numeroController = TextEditingController();
  TextEditingController bairroController = TextEditingController();
  TextEditingController referenciaController = TextEditingController();
  TextEditingController complementoController = TextEditingController();

  FocusNode nomeFocus = FocusNode();
  FocusNode focusLogradouro = FocusNode();
  FocusNode focusNumero = FocusNode();
  FocusNode focusBairro = FocusNode();
  FocusNode focusReferencia = FocusNode();
  FocusNode focusComplemento = FocusNode();

  NovoEnderecoController() {
    if (Get.arguments != null) {
      enderecoGeocode = Get.arguments['enderecoGeocode'];
      locationData = Get.arguments['locationData'];
      print('Location data vindo de enderecos: $locationData');
      marker = Marker(
          flat: true,
          markerId: MarkerId('marker'),
          onDragEnd: (LatLng latlng) {
            print('$latlng');
          },
          draggable: true,
          position: LatLng(
            locationData?.latitude ?? utilService.latitude,
            locationData?.longitude ?? utilService.longitude,
          ));
      // markers.add(marker);
      logradouroController.text = enderecoGeocode?.thoroughfare ?? '';
      bairroController.text = enderecoGeocode?.subLocality ?? '';
      /*  if (isNumeric(enderecoGeocode.featureName)) {
        numeroController.text = enderecoGeocode.featureName ?? '';
      } */
      cameraPosition = CameraPosition(
          //bearing: 192.8334901395799,
          target: LatLng(locationData?.latitude ?? utilService.latitude,
              locationData?.longitude ?? utilService.longitude),
          //tilt: 59.440717697143555,
          zoom: 16);
    } else {
      cameraPosition = CameraPosition(
          //bearing: 192.8334901395799,
          target: LatLng(utilService.latitude, utilService.longitude),
          //tilt: 59.440717697143555,
          zoom: 16);
    }
    getFutureLocation();
  }

  Future<Address> getFutureLocation([bool recursivo]) async {
    if (!(await requestLocationPermission())) {
      debugPrint('Nao autorizou');
      await location.requestPermission();
      return getFutureLocation(
          true); //Se o usuário não autorizar, já sai da funcao
    } else if (await location.hasPermission() == PermissionStatus.granted &&
        await location.serviceEnabled()) {
      locationData = await location.getLocation();
      LatLng latLng =
          LatLng(locationData?.latitude ?? 0, locationData?.longitude ?? 0);
      var newPosition = CameraPosition(target: latLng, zoom: 16);
      CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(newPosition);
      _mapController?.moveCamera(cameraUpdate);
      //setMarker(latLng);
      update();
      /*   debugPrint('Address: ${address.addressLine}'); */
      return address;
    } else {
      await location.requestService();
      getFutureLocation();
      update();
      return null;
    }
  }

  Future<bool> requestLocationPermission() async {
    bool hasPermission =
        await location.hasPermission() == PermissionStatus.granted ||
            await location.hasPermission() == PermissionStatus.grantedLimited;
    //Se o usuário já permitiu o acesso, retorne true
    if (hasPermission) return hasPermission;
    //Senão, solicite permissão
    PermissionStatus permissionStatus = await location.requestPermission();
    if (permissionStatus == PermissionStatus.granted ||
        permissionStatus == PermissionStatus.grantedLimited) return true;
    return false;
  }

  toggleSemNumero([bool sn]) {
    semNumero = sn ?? !semNumero;
    if (semNumero) numeroController.text = 'S/N';
  }

  onMapCreated(GoogleMapController _controller) {
    try {
      completerGoogleMap.complete(_controller);
      _mapController = _controller;
    } catch (e) {
      print('Erro ao criar mapa');
    }
  }

  toggleEditarLocalizacao() async {
    var result = await Get.toNamed(Routes.SELECIONA_LOCALIZACAO,
        arguments: {'localizacaoInicial': locationData});
    if (result != null && result['localizacaoSelecionada'] != null) {
      locationDataFinal = result['localizacaoSelecionada'];
      enderecoGeocode = result['enderecoLocation'];
      print('result $result');
      logradouroController.text = enderecoGeocode?.thoroughfare ?? '';
      bairroController.text = enderecoGeocode?.subLocality ?? '';
      nomeFocus.requestFocus();
    }
    LatLng latLng =
        LatLng(locationData?.latitude ?? 0, locationData?.longitude ?? 0);
    var newPosition = CameraPosition(target: latLng, zoom: 16);
    CameraUpdate cameraUpdate = CameraUpdate.newCameraPosition(newPosition);

    _mapController?.moveCamera(cameraUpdate);
    setMarker(latLng);
    /*  editarLocalizacao = !editarLocalizacao;
     */
  }

  salvarEndereco() async {
    if (formKey.currentState.validate() && locationData != null) {
      Endereco endereco = Endereco(
          nome: nomeController.text,
          logradouro: logradouroController.text,
          numero: numeroController.text,
          referencia: referenciaController.text,
          bairro: bairroController.text,
          lat: locationData?.latitude ?? 0,
          lng: locationData?.longitude ?? 0,
          padrao: false,
          id: 'temp');
      try {
        List<Endereco> enderecos = await repository.salvarEndereco(endereco);
        Get.back(result: {'enderecos': enderecos});
        utilService.snackBar(
            titulo: 'Sucesso!', mensagem: 'Endereço adicionado');
      } catch (e) {
        print('Erro: $e');
        utilService.snackBarErro();
      }
      print('Salvar Endereço');
    } else if (locationData == null) {
      utilService.snackBarErro(
          titulo: 'Localização não definida',
          mensagem:
              'Selecione uma localização para que possamos entregar o lanche');
    }
  }

  setMarker(LatLng latLng) {
    marker = Marker(
        markerId: MarkerId('marker'),
        //infoWindow: InfoWindow(title: 'marker_id_1', snippet: '*'),
        onDragEnd: setMarker,
        draggable: true,
        position: latLng);
    markers = [marker];
    update();
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    // ignore: deprecated_member_use
    return double.parse(s, (e) => null) != null;
  }
}

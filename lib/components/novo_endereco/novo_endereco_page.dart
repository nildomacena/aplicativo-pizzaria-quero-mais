import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/custom_widgets/map_preview.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_controller.dart';

class NovoEnderecoPage extends StatelessWidget {
  final NovoEnderecoController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cadastrar Novo Endereço'),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.location_on),
            onPressed: controller.getFutureLocation,
          )
        ],
      ),
      backgroundColor: Colors.grey[200],
      body: Container(
        child: Form(
          key: controller.formKey,
          child: ListView(
            children: <Widget>[
              Container(
                /*   width: Get.width,
                height: 200, */
                //color: Colors.blue,
                child: MapPreview(
                  height: 200,
                  width: Get.width,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(20, 30, 20, 10),
                child: Column(
                  children: <Widget>[
                    /* Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: TextFormField(
                        validator: (value) {
                          if (value.length == 0) return 'Campo obrigatório';
                          if (value.length < 3) return 'Digite um valor válido';
                        },
                        onChanged: (str) {},
                        onFieldSubmitted: (str) {
                          controller.focusLogradouro.requestFocus();
                        },
                        controller: controller.nomeController,
                        focusNode: controller.nomeFocus,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.words,
                        decoration: InputDecoration(
                            labelText: 'NOME',
                            hintText: 'Ex.: Casa da namorada'),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ), */
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: TextFormField(
                        focusNode: controller.focusLogradouro,
                        onChanged: (str) {
                          //setState(() {});
                        },
                        onFieldSubmitted: (str) {
                          controller.focusNumero.requestFocus();
                        },
                        textInputAction: TextInputAction.next,
                        validator: (value) {
                          if (value.length == 0) return 'Campo obrigatório';
                          if (value.length < 3) return 'Digite um valor válido';
                        },
                        controller: controller.logradouroController,
                        decoration: InputDecoration(labelText: 'LOGRADOURO'),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 50,
                              child: TextFormField(
                                onChanged: (str) {
                                  //setState(() {});
                                },
                                onFieldSubmitted: (str) {
                                  controller.focusBairro.requestFocus();
                                },
                                validator: (value) {
                                  if (!controller.semNumero) {
                                    if (value.length == 0)
                                      return 'Campo obrigatório';
                                  }
                                },
                                focusNode: controller.focusNumero,
                                enabled: !controller.semNumero,
                                controller: controller.numeroController,
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                    labelText: 'NÚMERO',
                                    labelStyle: TextStyle(fontSize: 14)),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Checkbox(
                              value: controller.semNumero,
                              onChanged: controller.toggleSemNumero),
                          GestureDetector(
                              onTap: controller.toggleSemNumero,
                              child: Text(
                                'Sem número',
                                style: TextStyle(fontSize: 16),
                              ))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              onChanged: (str) {},
                              textInputAction: TextInputAction.next,
                              onFieldSubmitted: (str) {
                                controller.focusComplemento.requestFocus();
                              },
                              controller: controller.bairroController,
                              focusNode: controller.focusBairro,
                              decoration: InputDecoration(labelText: 'BAIRRO'),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(padding: EdgeInsets.all(4)),
                          Expanded(
                            child: Container(
                              child: TextFormField(
                                onChanged: (str) {},
                                textInputAction: TextInputAction.next,
                                focusNode: controller.focusComplemento,
                                controller: controller.complementoController,
                                onFieldSubmitted: (str) {
                                  controller.focusReferencia.requestFocus();
                                },
                                decoration:
                                    InputDecoration(labelText: 'COMPLEMENTO'),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: TextFormField(
                        onChanged: (str) {},
                        textInputAction: TextInputAction.done,
                        focusNode: controller.focusReferencia,
                        controller: controller.referenciaController,
                        onFieldSubmitted: (str) {
                          controller.salvarEndereco();
                        },
                        decoration:
                            InputDecoration(labelText: 'PONTO DE REFERÊNCIA'),
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      height: 45,
                      margin: EdgeInsets.only(top: 20, bottom: 20),
                      padding: EdgeInsets.only(left: 20, right: 20),
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                          color: Get.theme.primaryColor,
                          textColor: Colors.white,
                          disabledTextColor: Colors.grey[600],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                              side: BorderSide(color: Colors.transparent)),
                          onPressed: controller.ocupado
                              ? null
                              : controller.locationDataFinal == null
                                  ? controller.toggleEditarLocalizacao
                                  : controller.salvarEndereco,
                          child: Text(
                            controller.locationDataFinal == null
                                ? 'SELECIONE A LOCALIZAÇÃO'
                                : controller.salvando
                                    ? "SALVANDO DADOS..."
                                    : "SALVAR",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                    )
                    /* Container(
                      margin: EdgeInsets.only(bottom: 20),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          TextField(
                            onChanged: (str) {
                              setState(() {});
                            },
                            controller:   numeroController,
                            decoration: InputDecoration(labelText: 'NUMERO'),
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          Checkbox(
                              value: controller.semNumero,
                              onChanged: (value) {
                                setState(() {
                                  controller.semNumero = value;
                                });
                              })
                        ],
                      ),
                    ), 
                    Container(
                        height: 100,
                        width: MediaQuery.of(context).size.width,
                        child: InkWell(
                          onTap: () {
                            print('Usar localização');
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.location_on,
                                color: Colors.blueGrey[300],
                                size: 40,
                              ),
                              Expanded(
                                  child: Container(
                                margin: EdgeInsets.only(
                                    top: 25, left: 5, bottom: 25),
                                height: double.infinity,
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Usar Localização atual',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    Text(
                                      'Ativar GPS',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                              ))
                            ],
                          ),
                        ))*/
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

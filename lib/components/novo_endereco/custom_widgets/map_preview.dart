import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_controller.dart';

class MapPreview extends StatelessWidget {
  final double height;
  final double width;
  final NovoEnderecoController controller = Get.find();
  MapPreview({@required this.height, @required this.width});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NovoEnderecoController>(
      builder: (_) {
        return Stack(
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: 200),
              height: controller.editarLocalizacao ? height + 150 : height,
              width: width,
              child: GoogleMap(
                buildingsEnabled: false,
                myLocationEnabled: true,
                compassEnabled: false,
                myLocationButtonEnabled: false,
                tiltGesturesEnabled: false,
                indoorViewEnabled: false,
                liteModeEnabled: true,
                mapToolbarEnabled: false,
                onTap: controller.setMarker,
                markers: Set<Marker>.of(controller.markers),
                mapType: MapType.terrain,
                initialCameraPosition: controller.cameraPosition,
                onMapCreated: controller.onMapCreated,
              ),
            ),
            Positioned(
                child: Container(
              height: height,
              width: width,
              color: Colors.transparent,
            )),
            Positioned(
                bottom: 0,
                right: (Get.width * .5) - 75,
                child: Container(
                  width: 150,
                  child: TextButton(
                    style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        backgroundColor: Get.theme.primaryColor,
                        primary: Colors.white),
                    onPressed: controller.toggleEditarLocalizacao,
                    child: Text(
                      'Definir a localização',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ))
          ],
        );
      },
    );
  }
}

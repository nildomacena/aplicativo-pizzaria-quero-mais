import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';

class NovoEnderecoRepository {
  final AuthProvider authProvider = Get.find();

  Future<List<Endereco>> salvarEndereco(Endereco endereco) {
    return authProvider.salvarEndereco(endereco);
  }
}

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class BottomSheetController extends GetxController {
  TextEditingController observacoesController = TextEditingController();

  onAvaliar() {
    Get.back(result: {
      'avaliar': true,
      'experiencia': observacoesController.text ?? ''
    });
  }
}

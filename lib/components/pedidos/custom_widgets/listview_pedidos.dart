import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';

class ListViewPedidos extends StatelessWidget {
  final PedidosController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    if (controller.pedidos.isEmpty)
      return Container(
        alignment: Alignment.center,
        height: Get.height,
        child: Text(
          'Sem pedidos anteriores',
          style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
        ),
      );
    return Container(
      child: ListView.builder(
        itemCount: controller.pedidos.length,
        itemBuilder: (BuildContext context, int index) {
          Pedido pedido = controller.pedidos[index];
          return Container(
            margin: EdgeInsets.all(10),
            child: Card(
              elevation: 5,
              child: Container(
                padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Data: ${pedido.dataHora}',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w300),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                        'Status: ${pedido.statusFormatado ?? 'PENDENTE'}',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w300),
                      ),
                    ),
                    Divider(),
                    Container(
                        margin: EdgeInsets.only(top: 10),
                        child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: pedido.entradas.length,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, int index2) {
                              return Container(
                                /*  height: 60,
                              width: 60, */
                                margin: EdgeInsets.only(bottom: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      height: 25,
                                      width: 25,
                                      margin: EdgeInsets.only(right: 10),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          border: Border.all(
                                              color:
                                                  Colors.grey.withOpacity(.5)),
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Text(
                                          pedido.entradas[index2].quantidade
                                              .toString(),
                                          style: TextStyle(fontSize: 17)),
                                    ),
                                    Expanded(
                                        child: Text(
                                            pedido.entradas[index2].item.nome,
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.w300)))
                                  ],
                                ),
                              );
                            })),
                    Divider(),
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Row(
                        children: <Widget>[
                          Text(
                            'Valor do Pedido: ',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w300),
                          ),
                          Text(
                            '${pedido.valorTotal?.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w300),
                          )
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      //height: 90,
                      margin: EdgeInsets.only(bottom: 20, top: 10),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              AutoSizeText(
                                'Avalie seu pedido: ',
                                style: TextStyle(
                                    fontSize: 17, fontWeight: FontWeight.w500),
                              ),
                              RatingBar(
                                itemSize: 20,
                                initialRating: pedido.avaliacao,
                                glowColor: Colors.yellow,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                ratingWidget: RatingWidget(
                                    full: Icon(
                                      Icons.star,
                                      color: Colors.yellow[700],
                                    ),
                                    half: Icon(
                                      Icons.star_half,
                                      color: Colors.yellow[700],
                                    ),
                                    empty: Icon(
                                      Icons.star_outline,
                                      color: Colors.yellow[700],
                                    )),
                                updateOnDrag: true,
                                tapOnlyMode: false,
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 4.0),
                                onRatingUpdate: (rating) async {
                                  //  avaliacao = rating;
                                  print(rating);
                                  /* await Future.delayed(Duration(seconds: 1));
                                  if (!Get.isBottomSheetOpen) */
                                  controller.showBottomSheetAvaliacao(
                                      pedido, rating);
                                },
                              ),
                            ],
                          ),
                          /* 
                                 TODO colocar uma forma do usuário enviar sua avaliação escrita sobre o pedido
                                  Container(
                                    margin: EdgeInsets.only(top: 10),
                                    child: FlatButton(
                                        textColor: Colors.red,
                                        onPressed: () async {
                                          bool salvou = await showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: Text(
                                                      'Descreva sua experiência'),
                                                  content: Container(
                                                      child: TextField(
                                                    controller:
                                                        _observacaoController,
                                                    maxLines: 3,
                                                    minLines: 1,
                                                    decoration: InputDecoration(
                                                        hintText:
                                                            'Descreve sua experiência'),
                                                  )),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context, false);
                                                        },
                                                        child:
                                                            Text('Cancelar')),
                                                    FlatButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context, true);
                                                        },
                                                        child: Text('OK'))
                                                  ],
                                                );
                                              });
                                          if (!salvou)
                                            _observacaoController.text = '';
                                        },
                                        child:
                                            Text('DESCREVA SUA EXPERIENCIA')),
                                  ), */
                          /* Container(
                                      child: TextField(
                                    //controller: _observacaoController,
                                    maxLines: 3,
                                    minLines: 1,
                                    decoration: InputDecoration(
                                        hintText: 'Descreve sua experiência'),
                                  )), */
                          /*   Expanded(child: Container()),
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: TextButton(
                                style: TextButton.styleFrom(
                                    primary: Get.theme.primaryColor),
                                onPressed: () async {
                                   controller.showBottomSheetAvaliacao(pedido);
                                  
                                },
                                child: Text('AVALIAR')),
                          ) */
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/custom_widgets/bottom_sheet_controller.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';

class BottomSheetAvaliacao extends StatelessWidget {
  final Pedido pedido;
  final PedidosController pedidosController;
  final BottomSheetController controller = Get.put(BottomSheetController());
  final double rating;
  BottomSheetAvaliacao(this.pedido, this.pedidosController, this.rating) {
    controller.observacoesController.text = '';
  }

  String get title =>
      rating < 3 ? 'Conte-nos o que aconteceu' : 'Descreva sua experência';
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: 230,
        width: Get.width,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                title,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: TextField(
                controller: controller.observacoesController,
                maxLines: 3,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: title,
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Get.theme.primaryColor, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Get.theme.primaryColor, width: 1.0),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: ElevatedButton(
                onPressed: controller.onAvaliar,
                child: Text('ENVIAR AVALIAÇÃO'),
                style:
                    ElevatedButton.styleFrom(primary: Get.theme.primaryColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}

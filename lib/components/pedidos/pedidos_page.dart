import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/custom_widgets/listview_pedidos.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_controller.dart';

class PedidosPage extends StatelessWidget {
  PedidosController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pedidos'),
        centerTitle: true,
      ),
      body: Container(
        child: GetBuilder<PedidosController>(
          builder: (_) {
            if (!controller.carregou)
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
                  backgroundColor: Get.theme.primaryColor,
                ),
              );
            else
              return ListViewPedidos();
          },
        ),
      ),
    );
  }
}

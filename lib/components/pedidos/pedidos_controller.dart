import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/custom_widgets/bottom_sheet_avaliacao.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class PedidosController extends GetxController {
  final PedidosRepository repository = Get.put(PedidosRepository());
  List<Pedido> pedidos = [];
  bool carregou = false;

  @override
  void onInit() async {
    super.onInit();
    pedidos = await repository.getPedidos();
    print('pedidos: $pedidos');
    carregou = true;
    update();
  }

  showBottomSheetAvaliacao(Pedido pedido, double rating) async {
    var result =
        await Get.bottomSheet(BottomSheetAvaliacao(pedido, this, rating));
    if (result != null && result['avaliar']) {
      try {
        await repository.avaliarPedido(
            pedido, rating, result['experiencia'] ?? '');
        utilService.snackBar(titulo: 'Avaliação enviada', mensagem: '');
      } catch (error) {
        printError();
        utilService.snackBarErro();
      }
    }
  }
}

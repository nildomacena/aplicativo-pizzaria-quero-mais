import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_controller.dart';

class PedidosBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PedidosController>(() => PedidosController());
  }
}

import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class PedidosRepository {
  final FirestoreProvider firestoreProvider = Get.find();
  final AuthProvider authProvider = Get.find();

  Future<List<Pedido>> getPedidos() {
    return authProvider.getPedidos();
  }

  Future<void> avaliarPedido(Pedido pedido, double rating, String experiencia) {
    return firestoreProvider.avaliarPedido(pedido, rating, experiencia);
  }
}

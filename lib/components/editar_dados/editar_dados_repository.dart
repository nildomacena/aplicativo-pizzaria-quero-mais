import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class EditarDadosRepository {
  final FirestoreProvider firestoreProvider = Get.find();
  final AuthProvider authProvider = Get.find();

  EditarDadosRepository();

  Future<Usuario> editarDadosPessoais(String nome, String celular) {
    return authProvider.salvarDadosPessoais(nome, celular);
  }
}

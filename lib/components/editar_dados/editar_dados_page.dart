import 'package:easy_mask/easy_mask.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_controller.dart';

class EditarDadosPage extends StatelessWidget {
  EditarDadosController controller = Get.put(EditarDadosController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Editar Dados'),
        ),
        body: Container(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10),
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: TextField(
                  onChanged: (str) {},
                  controller: controller.nomeController,
                  decoration: InputDecoration(labelText: 'NOME COMPLETO'),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: TextField(
                  onChanged: (str) {},
                  controller: controller.celularController,
                  keyboardType: TextInputType.phone,
                  inputFormatters: [TextInputMask(mask: '(99)99999-9999')],
                  decoration: InputDecoration(labelText: 'CELULAR'),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              Obx(() {
                return Container(
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                      color: Get.theme.primaryColor,
                      textColor: Colors.white,
                      disabledTextColor: Colors.grey[600],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.transparent)),
                      onPressed: !controller.formValido || controller.salvando
                          ? null
                          : () async {
                              await controller.salvarDadosPessoais();
                            },
                      child: Text(
                        controller.salvando ? "SALVANDO DADOS..." : "SALVAR",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      )),
                );
              }),
            ],
          ),
        )

        /* GetBuilder<EditarDadosController>(builder: (_) {
          return Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 10),
            child: ListView(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: TextField(
                    onChanged: (str) {},
                    controller: controller.nomeController,
                    decoration: InputDecoration(labelText: 'NOME COMPLETO'),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20),
                  child: TextField(
                    onChanged: (str) {},
                    controller: controller.celularController,
                    keyboardType: TextInputType.phone,
                    inputFormatters: [TextInputMask(mask: '(99)99999-9999')],
                    decoration: InputDecoration(labelText: 'CELULAR'),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                ),
                Container(
                  height: 60,
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                      color: Colors.red,
                      textColor: Colors.white,
                      disabledTextColor: Colors.grey[600],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.transparent)),
                      onPressed: controller.nomeController.text.length < 5 ||
                              controller.celularController.text.length < 8 ||
                              controller.salvando
                          ? null
                          : () async {
                              await controller.salvarDados();
                            },
                      child: Text(
                        controller.salvando ? "SALVANDO DADOS..." : "SALVAR",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      )),
                )
              ],
            ),
          );
        }) */
        );
  }
}

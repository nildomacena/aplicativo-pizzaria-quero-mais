import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_controller.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_repository.dart';

class EditarDadosBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(EditarDadosRepository());
    Get.lazyPut<EditarDadosController>(() => EditarDadosController());
  }
}

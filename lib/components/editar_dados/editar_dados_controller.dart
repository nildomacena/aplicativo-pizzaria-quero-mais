import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class EditarDadosController extends GetxController {
  final EditarDadosRepository repository = Get.put(EditarDadosRepository());
  TextEditingController nomeController = TextEditingController();
  TextEditingController celularController = TextEditingController();
  RxBool _formValido = RxBool(false);
  RxBool _salvando = RxBool(false);
  Usuario usuario;

  bool get formValido => _formValido.value;
  bool get salvando => _salvando.value;
  set salvando(bool value) => this._salvando.value = value;

  EditarDadosController() {
    usuario = Get.arguments['usuario'];
    nomeController.text = usuario?.nome ?? '';
    celularController.text = usuario?.celular ?? '';
    celularController.addListener(() {
      print('celularController.text.length: ${celularController.text.length}');
      if (celularController.text.length >= 14 && nomeController.text.isNotEmpty)
        _formValido.value = true;
      else
        _formValido.value = false;
    });
  }

  salvarDadosPessoais() async {
    try {
      await repository.editarDadosPessoais(
          nomeController.text, celularController.text);
      Get.back();
    } catch (e) {
      utilService.snackBarErro();
    }
  }
}

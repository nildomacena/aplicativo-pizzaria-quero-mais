import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Splashscreen extends StatelessWidget {
  Splashscreen() {
    Future.delayed(Duration(seconds: 2)).then((value) {
      print('splashscreen: $value');
      Get.offAllNamed(Routes.INITIAL);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: Get.height,
        width: Get.width,
        color: Colors.red,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              'assets/splashscreen.png',
              fit: BoxFit.cover,
            ),
            Positioned(
              bottom: 120,
              child: Container(
                height: 50,
                width: Get.width,
                alignment: Alignment.center,
                child: Container(
                  height: 50,
                  width: 50,
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                    backgroundColor: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

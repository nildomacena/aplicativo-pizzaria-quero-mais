import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/adicional.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class ItemDetailController extends GetxController {
  final ItemDetailRepository repository = Get.find();
  TextEditingController observacoesController = TextEditingController();
  CarrinhoController carrinhoController = Get.find();
  List<Adicional> adicionais = []; //Adicionais cadastrados
  List<Adicional> adicionaisSelecionados =
      []; //Adicionais selecionados para o item
  Map<String, bool> mapAdicionaisCheckbox = {};
  bool tecladoVisivel = false;
  int qtdAdicionar = 1;
  Item item;
  ItemDetailController() {
    initFuture();
    adicionaisSelecionados = [];
    item = Get.arguments['item'];
    print('ItemDetailController $item');

    /* KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        toggleTeclado(visible);
      },
    ); */
  }

  initFuture() async {
    adicionais = await repository.getAdicionais();
    adicionais.forEach((a) {
      mapAdicionaisCheckbox[a.id] = false;
    });
    print('adicionais: $adicionais');
    update();
  }

  toggleAdicional(Adicional adicional, bool value) {
    mapAdicionaisCheckbox[adicional.id] = value;
    if (!adicionaisSelecionados.remove(adicional)) {
      adicionaisSelecionados.add(adicional);
    }
    update();
  }

  recalculaValorTotal() {}

  double get valorTotal {
    double valorAdicionais = 0;
    if (adicionais != null) {
      adicionais.forEach((a) {
        if (mapAdicionaisCheckbox[a.id]) valorAdicionais += a.preco;
      });
    }
    return qtdAdicionar * (item?.preco ?? 0 + valorAdicionais);
  }

  toggleTeclado(bool visivel) {
    tecladoVisivel = visivel;
    update();
  }

  addQuantidade() {
    qtdAdicionar++;
    update();
  }

  subQuantidade() {
    if (qtdAdicionar == 1) return;
    qtdAdicionar--;
    update();
  }

  submitItem() {
    Entrada entrada = Entrada(item, qtdAdicionar,
        adicionais: adicionaisSelecionados,
        observacoes: observacoesController.text);
    print('Entrada: $entrada');
    carrinhoController.addEntrada(entrada);
    Get.back();
    utilService.snackBarItemAdicionado(item);
  }
}

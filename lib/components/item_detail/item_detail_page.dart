import 'package:expandable/expandable.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/adicional.model.dart';

class ItemDetailPage extends StatelessWidget {
  final ItemDetailController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    Widget rowQuantidade() {
      return GetBuilder<ItemDetailController>(builder: (_) {
        return Container(
          width: double.infinity,
          height: 100,
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AnimatedOpacity(
                opacity: controller.qtdAdicionar > 1 ? 1 : 0,
                duration: Duration(milliseconds: 700),
                child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(30),
                      onTap: controller.qtdAdicionar <= 1
                          ? null
                          : () {
                              print('Diminuir quantidade');
                              if (controller.qtdAdicionar > 1)
                                controller.subQuantidade();
                            },
                      child: Container(
                        height: 45,
                        width: 45,
                        decoration: BoxDecoration(
                            color: controller.qtdAdicionar == 1
                                ? Colors.grey[300]
                                : Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(color: Colors.grey)),
                        child: Icon(
                          Icons.remove,
                          color: controller.qtdAdicionar == 1
                              ? Colors.grey
                              : Colors.black,
                        ),
                      ),
                    )),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15),
                child: Text(
                  controller.qtdAdicionar.toString(),
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
                ),
              ),
              Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {
                      controller.addQuantidade();

                      print('Aumentar quantidade');
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey)),
                      child: Icon(Icons.add),
                    ),
                  )),
            ],
          ),
        );
      });
    }

    Widget adicionais() {
      return GetBuilder<ItemDetailController>(builder: (_) {
        if (controller.adicionais == null)
          return Center(
            child: CircularProgressIndicator(),
          );
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 15),
          child: ExpandablePanel(
            collapsed: Container(
                /*  
             Ver se é interessante colocar esse "preview"
             child: CheckboxListTile(
                activeColor: Colors.black,
                title: Text('${controller.adicionais[0].nome}'),
                value: controller
                    .mapAdicionaisCheckbox[controller.adicionais[0].id],
                onChanged: (value) {
                  controller.toggleAdicional(controller.adicionais[0], value);
                },
              ), */
                ),
            expanded: Container(
              padding: EdgeInsets.only(left: 10),
              child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: controller.adicionais.length,
                itemBuilder: (context, index) {
                  Adicional adicional = controller.adicionais[index];
                  return CheckboxListTile(
                      activeColor: Colors.black,
                      value: controller.mapAdicionaisCheckbox[adicional.id],
                      title: Text(
                          '${adicional.nome} - R\$${adicional.preco.toStringAsFixed(2)}'),
                      onChanged: (value) {
                        controller.toggleAdicional(adicional, value ?? false);
                      });
                },
              ),
            ),
            header: Container(
                color: Colors.grey[400],
                //height: double.infinity,
                height: 40,
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(left: 10),
                child: Text(
                  'ADICIONAIS',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                )),
          ),
        );
      });
    }

    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                  title: Text(
                    controller.item?.nome ?? '',
                    style: TextStyle(
                      shadows: <Shadow>[
                        Shadow(
                            offset: Offset(2.0, 2.0),
                            blurRadius: 3.0,
                            color: Colors.black),
                        Shadow(
                            offset: Offset(2.0, 2.0),
                            blurRadius: 3.0,
                            color: Colors.black),
                      ],
                    ),
                  ),
                  floating: true,
                  pinned: true,
                  flexibleSpace: ExtendedImage.network(
                    controller.item?.imagemUrl ??
                        'https://i1.wp.com/www.maceioalagoas.com/wp-content/uploads/2018/03/pizza-1-2.jpg?fit=1024%2C640&ssl=1',
                    fit: BoxFit.cover,
                  ),
                  // Make the initial height of the SliverAppBar larger than normal.
                  expandedHeight: 200,
                ),
                SliverList(
                  // Use a delegate to build items as they're scrolled on screen.
                  delegate: SliverChildBuilderDelegate(
                    // The builder function returns a ListTile with a title that
                    // displays the index of the current item.

                    (context, index) {
                      if (index == 0)
                        return Container(
                            margin:
                                EdgeInsets.only(top: 10, left: 10, right: 10),
                            child: Text(
                              controller.item?.descricao ?? '',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w300),
                            ));
                      if (index == 1) return adicionais();
                      if (index == 2)
                        return Container(
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                padding: EdgeInsets.only(left: 10),
                                color: Colors.grey[400],
                                alignment: Alignment.centerLeft,
                                height: 40,
                                width: Get.width,
                                child: Text(
                                  'OBSERVAÇÕES',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: TextField(
                                  controller: controller.observacoesController,
                                  maxLines: 3,
                                  textInputAction: TextInputAction.done,
                                  decoration: InputDecoration(
                                    hintText:
                                        'Ex. Sem presunto. Adicionar Molho',
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 1.0),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      if (index == 3) return rowQuantidade();
                      return ListTile(title: Text('Item #$index'));
                    },
                    // Builds 1000 ListTiles
                    childCount: 4,
                  ),
                ),
              ],
            ),
          ),
          GetBuilder<ItemDetailController>(builder: (_) {
            return AnimatedContainer(
              //opacity: controller.tecladoVisivel ? 0 : 1,
              //
              height: controller.tecladoVisivel ? 0 : 80,
              duration: Duration(milliseconds: 200),
              child: Container(
                width: Get.width,
                child: Material(
                  elevation: 10,
                  child: Container(
                    padding: EdgeInsets.only(
                        top: 10, bottom: 10, left: 15, right: 15),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(width: 1, color: Colors.grey),
                      ),
                    ),
                    child: TextButton(
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.black,
                          shadowColor: Colors.white),
                      child: GetBuilder<ItemDetailController>(
                        builder: (_) {
                          return Text(
                            'Adicionar ${controller.qtdAdicionar} ao Carrinho - R\$${controller.valorTotal.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          );
                        },
                      ),
                      onPressed: () {
                        controller.submitItem();
                      },
                    ),
                  ),
                ),
              ),
            );
          })
        ],
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/adicional.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class ItemDetailRepository {
  final FirestoreProvider firestoreProvider = Get.find();

  ItemDetailRepository();

  Future<List<Adicional>> getAdicionais() {
    return firestoreProvider.getAdicionais();
  }
}

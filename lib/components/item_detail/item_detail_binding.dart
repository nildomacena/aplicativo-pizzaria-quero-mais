import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_controller.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_repository.dart';

class ItemDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemDetailRepository>(() => ItemDetailRepository());
    Get.lazyPut<ItemDetailController>(() => ItemDetailController());
  }
}

import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';

class CarrinhoBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CarrinhoController>(() => CarrinhoController());
  }
}

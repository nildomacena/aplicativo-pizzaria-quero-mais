import 'package:app_pizzaria_quero_mais/data/model/pizza.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_repository.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/pagamento_modal.dart/pagamento_modal.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class CarrinhoController extends GetxController {
  final CarrinhoRepository repository = Get.put(CarrinhoRepository());
  Endereco enderecoEntrega;
  Usuario usuario;
  RxInt _frete = 0.obs;
  double cashback;
  double cashbackUsuario = 0;
  //String textoBotao = 'SELECIONE UMA FORMA DE PAGAMENTO';
  RxList<Entrada> _entradas = RxList<Entrada>();
  RxList<Endereco> _enderecos = RxList<Endereco>();
  RxList<Pizza> _pizzas = RxList<Pizza>();

  TextEditingController observacoesController = TextEditingController();
  FormaPagamento formaPagamento;
  final RxInt _quantidade = RxInt(0);
  final RxDouble _valorTotal = RxDouble(0);

  List<Endereco> get enderecos => _enderecos.toList();
  set enderecos(List<Endereco> e) => _enderecos.value = e;

  double get frete => _frete.value.toDouble();
  set frete(double f) => _frete.value = f.toInt();

  double get valorCashback => (valorTotal - frete) * cashback;
  List<Pizza> get pizzas => _pizzas.toList();
  set pizzas(List<Pizza> p) => _pizzas.value = p;

  @override
  onInit() async {
    cashback = await repository.cashback;
    _entradas.listen((entradas) {
      print('Entradas listen: $entradas');
      updateValorTotal();
    });
    _pizzas.listen((pizzas) {
      print('Pizzas listen: $pizzas');
      updateValorTotal();
    });
    cashbackUsuario = await repository.cashbackUsuario ?? 0;
    print('Cashback usuario: ${cashbackUsuario ?? 0}');
    _entradas.value = [];
    super.onInit();
    print('Cashback: $cashback');
  }

  set entradas(value) => this._entradas.value = value;
  List<Entrada> get entradas => this._entradas.toList();
  set valorTotal(value) => this._valorTotal.value = value;
  double get valorTotal => this._valorTotal.value - cashbackUsuario ?? 0;
  set quantidade(value) => this._quantidade.value = value;
  int get quantidade => this._quantidade.value;

  double get valorSemFrete => valorTotal - frete + cashbackUsuario;

  addEntrada(Entrada entrada) {
    _entradas.add(entrada);
  }

  String get textoBotao => enderecoEntrega == null
      ? 'SELECIONAR O ENDEREÇO'
      : formaPagamento == null
          ? 'SELECIONE UMA FORMA DE PAGAMENTO'
          : 'FINALIZAR PEDIDO';

  /*  updateValorTotal() {
    _entradas.listen((e) {
      print('e: $e');
      double valAux = 0;
      int qtdAux = 0;
      e.forEach((Entrada entrada) {
        valAux += entrada.precoTotal;
        qtdAux += entrada.quantidade;
      });
      valorTotal = valAux + frete;
      quantidade = qtdAux;
      print(
          'Valor total: $valorTotal - Quantidade: $quantidade - Frete: $frete');
    });
  } */

  updateValorTotal() async {
    int qtdAux = 0;
    double valAux = 0;
    valorTotal = 0.toDouble();
    entradas.forEach((e) {
      valAux += e.precoTotal;
      qtdAux += e.quantidade;
    });
    pizzas.forEach((element) {
      print('pizza ${element.preco}');
      valAux += element.preco;
      qtdAux++;
    });
    valorTotal = valAux + frete;
    quantidade = qtdAux;
    print(
        'Valor total: $valorTotal - Pizza: ${pizzas?.length} - Quantidade: $quantidade - Frete: $frete');
    cashbackUsuario = await repository.cashbackUsuario ?? 0;
    update();
  }

  listenerCarrinho() {
    _pizzas.listen((p) {
      print('pizzas.listen $p');
      updateValorTotal();
    });
    _entradas.listen((e) {
      updateValorTotal();
    });
    _frete.listen((f) {
      print('F: $f');
      updateValorTotal();
    });
  }

  String adicionaisToString(Entrada entrada) {
    String string = 'Adicionais: ';
    String aux = '';
    entrada.adicionais.forEach((e) {
      string += e.nome + ', ';
    });
    string = string.substring(0, string.lastIndexOf(','));
    int ultimaVirgula = string.lastIndexOf(',');
    if (ultimaVirgula > 0) {
      aux = string.substring(0, ultimaVirgula) +
          ' e' +
          string.substring(ultimaVirgula + 1);
    }
    //string = string.substring(0, string.lastIndexOf(','));
    return aux;
  }

  printItem(Entrada entrada) {
    print(
        '${entrada.item} - ${entrada.adicionais} - Preço total: ${entrada.precoTotal} - Preço mais adicionais: ${entrada.precoItemMaisAdicionais} ');
  }

  selectEndereco() async {
    var result = await Get.toNamed(Routes.ENDERECOS,
        arguments: {'veioDoCarrinho': true});
    if (result != null && result['endereco'] != null)
      enderecoEntrega = result['endereco'];

    if (enderecoEntrega != null)
      frete = ((enderecoEntrega?.distance ?? 0 * repository.fretePorKm))
          .round()
          .toDouble();
    print('selectEndereco() Frete $frete');
    updateValorTotal();
    update();
  }

  deleteEntrada(Entrada entrada) async {
    var result = await Get.dialog(AlertDialog(
        title: Text('Confirmação'),
        content:
            Text('Deseja realmente remover ${entrada.item.nome} do carrinho?'),
        actions: [
          TextButton(
            onPressed: () {
              Get.back(result: false);
            },
            child: Text('CANCELAR'),
          ),
          TextButton(
            onPressed: () {
              Get.back(result: true);
            },
            child: Text('CONFIRMAR'),
          )
        ]));
    print('resultsad: $result');
    if (result == null || !result) return;
    print(_entradas.remove(entrada));
    if (entradas.isEmpty && pizzas.isEmpty) Get.back();
    updateValorTotal();
    update();
  }

  deletePizza(Pizza pizza) async {
    var result = await Get.dialog(AlertDialog(
        title: Text('Confirmação'),
        content: Text('Deseja realmente remover essa pizza do carrinho?'),
        actions: [
          TextButton(
            onPressed: () {
              Get.back(result: false);
            },
            child: Text('CANCELAR'),
          ),
          TextButton(
            onPressed: () {
              Get.back(result: true);
            },
            child: Text('CONFIRMAR'),
          )
        ]));

    if (result == null || !result)
      return;
    else {
      print('result: $result');

      print(_pizzas.remove(pizza));
      updateValorTotal();
      if (entradas.isEmpty && pizzas.isEmpty) Get.back();
      update();
    }
  }

  selecionarPagamento() async {
    dynamic result =
        await Get.to(() => PagamentoModal(), fullscreenDialog: true);
    if (result != null) formaPagamento = result['formaPagamento'];
    update();
  }

  finalizarPedido() async {
    try {
      if (!(await repository.checkInfoUsuario)) {
        // Se o usuário não preencheu todo seu cadastro
        Get.back();
        utilService.snackBar(
            titulo: 'Preencha seu dados',
            mensagem:
                'Para finalizar seu pedido, preencha seu cadastro com nome e telefone');
        return;
      }
      utilService.showAlertCarregando('Salvando pedido...');
      Usuario usuario = await repository.usuario;
      await repository.finalizarPedido(
          entradas,
          usuario,
          observacoesController.text,
          enderecoEntrega,
          formaPagamento,
          valorTotal,
          valorSemFrete,
          cashbackUsuario,
          frete.toInt());
      utilService.fecharAlert();
      Get.back();
      utilService.snackBar(
          titulo: 'Pedido finalizado',
          mensagem:
              'Seu pedido foi finalizado. Você será informado quando ele for atualizado');
      entradas = <Entrada>[];
      pizzas = <Pizza>[];
      observacoesController.text = '';
      enderecoEntrega = null;
      formaPagamento = null;
      utilService.snackBar(
          titulo: 'Pedido enviado!',
          mensagem: 'Você será atualizado a cada passo do seu pedido');
    } catch (e) {
      print('Erro ao salvar pedido: $e');
      utilService.snackBarErro();
    }
  }

  addPizza(Pizza pizza) {
    _pizzas.add(pizza);
  }
}

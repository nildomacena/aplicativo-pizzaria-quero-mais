import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class CarrinhoRepository {
  final FirestoreProvider firestoreProvider = Get.find();
  final AuthProvider authProvider = Get.find();
  List<Endereco> get enderecos$ => authProvider.enderecos;

  Future<Usuario> get usuario => authProvider.getCurrentUsuario();

  Future<dynamic> finalizarPedido(
      List<Entrada> entradas,
      Usuario usuario,
      String observacoes,
      Endereco endereco,
      FormaPagamento formaPagamento,
      double valorTotal,
      double valorTotalSemFrete,
      double cashbackUtilizado,
      int frete) {
    return firestoreProvider.finalizarPedido(
        entradas,
        usuario,
        observacoes,
        endereco,
        formaPagamento,
        valorTotal,
        valorTotalSemFrete,
        cashbackUtilizado,
        frete);
  }

  double get fretePorKm => firestoreProvider.fretePorKm;
  Future<double> get cashback => firestoreProvider.getCashback();
  Future<double> get cashbackUsuario => authProvider.getCashbackUsuario();
  Future<bool> get checkInfoUsuario => authProvider.checkInfoUsuario();
}

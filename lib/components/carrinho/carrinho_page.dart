import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/container_observacoes.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/botao_fechar_pedido.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/container_endereco.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/container_pagamento.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/container_resumo.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/custom_widgets/list_itens.dart';

class CarrinhoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Carrinho'),
      ),
      body: Container(
          child: Column(
        children: [
          Expanded(
            child: ListView(
              shrinkWrap: true,
              children: [
                ContainerEndereco(),
                ListItens(),
                ContainerPagamento(),
                ContainerObservacoes(),
                Divider(),
                ContainerResumo()
              ],
            ),
          ),
          BotaoFecharPedido()
        ],
      ) /* Column(
          children: [
            Expanded(
              child: Container(
                child: ListView(
                  children: [ContainerEndereco()],
                ),
              ),
            ),
            Container(
              height: 80,
              width: Get.width,
              decoration: BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey)),
                color: Colors.white,
              ),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: ElevatedButton(
                  child: AutoSizeText(
                    'SELECIONE UMA FORMA DE PAGAMENTO',
                    maxLines: 1,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                  style: ElevatedButton.styleFrom(
                    /* color: Colors.red,
                    textColor: Colors.white,
                    disabledTextColor: Colors.grey[600], */

                    primary: Colors.grey[600],
                    shadowColor: Colors.green,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  onPressed: () {}),
            )
          ],
        ), */
          ),
    );
  }
}

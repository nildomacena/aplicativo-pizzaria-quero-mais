import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';

class ContainerResumo extends StatelessWidget {
  CarrinhoController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CarrinhoController>(builder: (_) {
      return Container(
        margin: EdgeInsets.only(top: 0, bottom: 20),
        alignment: Alignment.center,
        height: 195,
        color: Colors.white,
        child: Material(
          elevation: 2,
          child: Column(
            children: <Widget>[
              Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        'Total dos itens:',
                        style: TextStyle(fontSize: 17),
                      )),
                      Text(
                        'R\$${controller.valorSemFrete.toStringAsFixed(2)}',
                        style: TextStyle(fontSize: 17),
                      ),
                    ],
                  )),
              Container(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        'Valor da entrega:',
                        style: TextStyle(fontSize: 17),
                      )),
                      Text(
                        controller.enderecoEntrega == null
                            ? 'Selecione o endereço'
                            : 'R\$${controller.frete.toStringAsFixed(2)}',
                        style: TextStyle(fontSize: 17),
                      ),
                    ],
                  )),
              if (controller.cashbackUsuario != null &&
                  controller.cashbackUsuario > 0)
                Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Desconto cashback:',
                          style: TextStyle(fontSize: 17),
                        )),
                        Text(
                          '- R\$${controller.cashbackUsuario.toStringAsFixed(2)}',
                          style: TextStyle(fontSize: 17),
                        ),
                      ],
                    )),
              /*   Container(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        'Valor da entrega:',
                        style: TextStyle(fontSize: 17),
                      )),
                      Text(
                        controller.enderecoEntrega == null
                            ? 'Selecione o endereço'
                            : 'R\$${controller.frete.toStringAsFixed(2)}',
                        style: TextStyle(fontSize: 17),
                      ),
                    ],
                  )), */
              /*  if (controller.enderecoEntrega != null)
                Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Entrega:',
                          style: TextStyle(fontSize: 17),
                        )),
                        Text(
                          'R\$${carrinhoProvider.valorEntrega(enderecoEntrega).toStringAsFixed(2)}',
                          style: TextStyle(fontSize: 17),
                        ),
                      ],
                    )), */
              Container(
                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: Text(
                        'Valor total do pedido:',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      )),
                      Text(
                        'R\$${controller.valorTotal.toStringAsFixed(2)}',
                        //'R\$${carrinhoProvider.valorTotal(enderecoEntrega).toStringAsFixed(2)}',
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                    ],
                  )),
              Expanded(
                child: Container(),
              ),
              if (controller.valorCashback != null &&
                  controller.valorCashback > 0)
                Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text(
                          'Desconto para a próxima compra:',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w400),
                        )),
                        Text(
                          'R\$${controller.valorCashback.toStringAsFixed(2)}',
                          //'R\$${carrinhoProvider.valorTotal(enderecoEntrega).toStringAsFixed(2)}',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                      ],
                    )),
            ],
          ),
        ),
      );
    });
  }
}

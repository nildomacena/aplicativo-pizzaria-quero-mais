import 'package:app_pizzaria_quero_mais/data/model/pizza.model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';

class ListItens extends StatelessWidget {
  final CarrinhoController controller = Get.find();

  Widget entradaListItem(
    Entrada entrada,
  ) {
    return GestureDetector(
      onTap: () {
        controller.printItem(entrada);
      },
      child: Material(
        elevation: 3,
        color: Colors.white,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Text(
                        '${entrada.quantidade.toString()}x - ${entrada.item.nome}',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500)),
                  ),
                  Text(
                      'R\$${(entrada.precoItemMaisAdicionais).toStringAsFixed(2)}',
                      textAlign: TextAlign.end,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  Material(
                    color: Colors.transparent,
                    type: MaterialType.circle,
                    borderOnForeground: false,
                    child: IconButton(
                        icon: Icon(
                          Icons.delete,
                          size: 20,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          controller.deleteEntrada(entrada);
                        }),
                  )
                ],
              ),
              if (entrada.adicionais != null && entrada.adicionais.isNotEmpty)
                Container(
                  padding: EdgeInsets.only(left: 0, top: 5),
                  width: double.infinity,
                  child: Text(controller.adicionaisToString(entrada),
                      style: TextStyle(
                        fontSize: 18,
                      )),
                ),
              if (entrada.observacoes != null && entrada.observacoes.isNotEmpty)
                Container(
                  padding: EdgeInsets.only(left: 0, top: 5),
                  width: double.infinity,
                  child: Text('Instruções: ${entrada.observacoes}',
                      style: TextStyle(
                        fontSize: 18,
                      )),
                ),
              Divider(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget pizzaListItem(
    Pizza pizza,
  ) {
    return GestureDetector(
      onTap: () {
        //  controller.printItem(pizza);
      },
      child: Material(
        elevation: 3,
        color: Colors.white,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: AutoSizeText('${pizza.descricao} ----------- ',
                        maxLines: 1,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500)),
                  ),
                  Text('R\$${(pizza.preco).toStringAsFixed(2)}',
                      textAlign: TextAlign.end,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500)),
                  Material(
                    color: Colors.transparent,
                    type: MaterialType.circle,
                    borderOnForeground: false,
                    child: IconButton(
                        icon: Icon(
                          Icons.delete,
                          size: 20,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          controller.deletePizza(pizza);
                        }),
                  )
                ],
              ),
              Divider(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Container(
            padding: EdgeInsets.only(top: 20, left: 15),
            width: MediaQuery.of(context).size.width,
            height: 75,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Itens do Pedido',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ],
            )),
        GetBuilder<CarrinhoController>(builder: (_) {
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: controller.entradas.length,
                  itemBuilder: (context, index) {
                    return entradaListItem(controller.entradas[index]);
                  }),
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: controller.pizzas.length,
                  itemBuilder: (context, index) {
                    return pizzaListItem(controller.pizzas[index]);
                  })
            ],
          );
        })
      ],
    ));
  }
}

import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContainerObservacoes extends StatelessWidget {
  final CarrinhoController carrinhoController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            padding: EdgeInsets.only(left: 10),
            alignment: Alignment.centerLeft,
            height: 40,
            width: Get.width,
            child: Text(
              'OBSERVAÇÕES',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 0),
            padding: EdgeInsets.only(left: 10, right: 10),
            child: TextField(
              controller: carrinhoController.observacoesController,
              maxLines: 2,
              textInputAction: TextInputAction.done,
              decoration: InputDecoration(
                hintText: 'Ex. Sem presunto. Adicionar Molho',
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 1.0),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 1.0),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

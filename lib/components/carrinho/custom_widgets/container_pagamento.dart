import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';

class ContainerPagamento extends StatelessWidget {
  final CarrinhoController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CarrinhoController>(builder: (_) {
      return Container(
        margin: EdgeInsets.only(top: 20, bottom: 0),
        alignment: Alignment.center,
        height: 80,
        child: Material(
          elevation: 2,
          child: Container(
              color: Colors.white,
              height: double.infinity,
              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'FORMA DE PAGAMENTO:',
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                      ),
                      if (controller.formaPagamento != null)
                        Container(
                          margin: EdgeInsets.only(right: 10, top: 5),
                          child: AutoSizeText(
                            '${controller.formaPagamento?.descricaoCompleta}',
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w300),
                          ),
                        ),
                      /*  if (controller.formaPagamento != null)
                      Container(
                        child: Text('${controller.formaPagamento.descricao}'),
                      ) */
                    ],
                  )),
                  TextButton(
                    style: TextButton.styleFrom(
                        backgroundColor: Get.theme.primaryColor
                        //
                        ),
                    onPressed: () {
                      controller.selecionarPagamento();
                    },
                    child: Text(
                      'SELECIONAR',
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              )),
        ),
      );
    });
  }
}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';

class ContainerEndereco extends StatelessWidget {
  final CarrinhoController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 4,
      child: InkWell(
          splashColor: Colors.black38,
          onTap: controller.selectEndereco,
          child: GetBuilder<CarrinhoController>(
            builder: (_) {
              return Container(
                padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                height: 70,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: controller.enderecoEntrega == null
                          ? Center(
                              child: Container(
                                alignment: Alignment.center,
                                height: 60,
                                width: MediaQuery.of(context).size.width,
                                child: AutoSizeText(
                                  "Clique aqui para selecionar o endereço",
                                  //controller.enderecoEntrega == null? 'SELECIONAR ENDERECO':
                                  /*  controller.usuario != null &&
                                    controller.usuario.enderecos != null &&
                                    controller.usuario.enderecos?.length > 0
                                ? "CLIQUE AQUI E SELECIONE UM ENDEREÇO"
                                : "CADASTRAR ENDEREÇO", */
                                  maxLines: 1,
                                  style: TextStyle(
                                      //decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.w500,
                                      decoration: TextDecoration.underline,
                                      fontSize: 20,
                                      color: Colors.blue),
                                ),
                              ),
                            )
                          : Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    child: Text('Endereço para entrega'),
                                  ),
                                  /*   Container(
                                      margin: EdgeInsets.only(top: 7),
                                      child: Text(
                                          controller.enderecoEntrega?.nome ??
                                              '',
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold))), */
                                  Container(
                                      margin: EdgeInsets.only(top: 3),
                                      child: Text(
                                          '${controller.enderecoEntrega?.logradouro}, ${controller.enderecoEntrega?.numero ?? 'S/N'} - ${controller.enderecoEntrega?.bairro}',
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w300)))
                                ],
                              ),
                            ),
                    ),
                    if (controller.enderecoEntrega != null)
                      Container(
                          child: Icon(Icons.arrow_forward_ios,
                              size: 20, color: Get.theme.primaryColor))
                  ],
                ),
              );
            },
          )),
    );
  }
}

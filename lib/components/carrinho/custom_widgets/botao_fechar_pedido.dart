import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';

class BotaoFecharPedido extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CarrinhoController>(builder: (_) {
      return Container(
        height: 80,
        width: Get.width,
        decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey)),
          color: Colors.white,
        ),
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: ElevatedButton(
            child: AutoSizeText(
              _.textoBotao,
              maxLines: 1,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
            ),
            style: ElevatedButton.styleFrom(
              /* color: Colors.red,
                    textColor: Colors.white,
                    disabledTextColor: Colors.grey[600], */

              primary: Get.theme.primaryColor,
              shadowColor: Colors.green,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  side: BorderSide(color: Colors.transparent)),
            ),
            onPressed: _.enderecoEntrega == null || _.formaPagamento == null
                ? null
                : _.finalizarPedido),
      );
    });
  }
}

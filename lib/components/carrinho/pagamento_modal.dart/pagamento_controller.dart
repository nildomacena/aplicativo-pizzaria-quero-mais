import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';

class PagamentoController extends GetxController {
  FormaPagamento formaSelecionada;
  TextEditingController trocoController = TextEditingController();
  List<FormaPagamento> formasPagamento = [
    FormaPagamento('Crédito', Icons.credit_card),
    FormaPagamento('Débito', Icons.credit_card),
    FormaPagamento('Dinheiro', Icons.attach_money)
  ];

  selectFormaPagamento(FormaPagamento formaPagamento) {
    trocoController.text = '';
    if (formaSelecionada != null && formaSelecionada == formaPagamento) {
      formaSelecionada = null;
    } else
      formaSelecionada = formaPagamento;
    update();
  }

  selecionarPagamento() {
    formaSelecionada?.observacao = trocoController.text;
    Get.back(result: {
      'formaPagamento': formaSelecionada,
      'troco': trocoController.text
    });
  }
}

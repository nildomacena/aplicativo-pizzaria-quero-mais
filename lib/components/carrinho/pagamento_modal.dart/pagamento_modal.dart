import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/pagamento_modal.dart/pagamento_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';

class PagamentoModal extends StatelessWidget {
  final PagamentoController controller = Get.put(PagamentoController());

  Widget formaPagamentoTile(FormaPagamento formaPagamento) {
    return GetBuilder<PagamentoController>(
      builder: (_) {
        return ListTile(
          selectedTileColor: Colors.grey[300],
          selected: formaPagamento == controller.formaSelecionada,
          onTap: () {
            controller.selectFormaPagamento(formaPagamento);
          },
          leading: Icon(formaPagamento.iconData),
          title: Text(
            formaPagamento.descricao,
            style: TextStyle(
                fontSize: 20,
                fontWeight: formaPagamento == controller.formaSelecionada
                    ? FontWeight.bold
                    : FontWeight.normal),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: AutoSizeText(
            'Selecione a forma de pagamento',
            maxLines: 1,
          ),
        ),
        body: GetBuilder<PagamentoController>(
          builder: (_) {
            return ListView(
              children: [
                ListView(
                    shrinkWrap: true,
                    children: controller.formasPagamento
                        .map((f) => formaPagamentoTile(f))
                        .toList()),
                if (controller.formaSelecionada != null &&
                    controller.formaSelecionada?.descricao == 'Dinheiro')
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    height: 100,
                    width: Get.width,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            controller: controller.trocoController,
                            style: TextStyle(fontSize: 30),
                            decoration: InputDecoration(
                                labelText: 'Troco para: ',
                                //hintText: 'Troco para...',
                                prefix: Text('R\$')),
                            keyboardType: TextInputType.number,
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  margin: EdgeInsets.only(top: 25, left: 20, right: 20),
                  padding: EdgeInsets.only(top: 10),
                  width: Get.width,
                  height: 70,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Colors.black),
                    child: Text(
                      'Selecionar Pagamento',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      controller.selecionarPagamento();
                    },
                  ),
                )
              ],
            );
          },
        ));
  }
}

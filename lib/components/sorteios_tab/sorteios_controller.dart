import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class SorteiosController extends GetxController {
  RxList<Sorteio> _sorteiosPendentes = RxList<Sorteio>();
  RxList<Sorteio> _resultadoSorteios = RxList<Sorteio>();
  Rx<Usuario> _usuario = Rx<Usuario>(null);

  set sorteiosPendentes(List<Sorteio> value) =>
      this._sorteiosPendentes.value = value;
  List<Sorteio> get sorteiosPendentes => this._sorteiosPendentes.toList();

  set resultadoSorteios(List<Sorteio> value) =>
      this._resultadoSorteios.value = value;
  List<Sorteio> get resultadoSorteios => this._resultadoSorteios.toList();

  Usuario get usuario => _usuario.value;

  SorteiosController() {
    _sorteiosPendentes.bindStream(repository.streamSorteiosPendentes());
    _resultadoSorteios.bindStream(repository.resultadosSorteios());
    _sorteiosPendentes.listen((p) {
      print('SorteiosPendentes: $p');
    });
    _usuario.bindStream(repository.streamUsuario());
  }

  final SorteiosRepository repository = Get.put(SorteiosRepository());

  Future<bool> checkUsuarioLogado() async {
    if (usuario != null) return true;
    var result = await Get.dialog(AlertDialog(
      title: Text('Faça login para participar'),
      content: Text(
          'Para participar dos nosso sorteios e ter acesso a todas as funcionalidades, faça login com sua conta Google'),
      actions: [
        TextButton.icon(
          icon: Icon(FontAwesome5Brands.google),
          label: Text('LOGIN COM GOOGLE'),
          onPressed: () async {
            try {
              await repository.fazerLogin();
              Get.back(result: true);
              utilService.snackBar(
                  titulo: 'Sucesso',
                  mensagem: 'Voce esta logado como: ${usuario.email}');
            } catch (e) {
              Get.back();
              print('Erro durante o login: $e');
              utilService.snackBarErro(
                  mensagem: 'Ocorreu um erro durante o login');
            }
          },
        )
      ],
    ));
    if (result != null) return result;
    return false; // Testes
  }

  onParticiparSorteio(Sorteio sorteio) async {
    try {
      if (!await checkUsuarioLogado()) {
        return;
      }
      await alertInstrucoes(sorteio, false);
      utilService.showAlertCarregando();
      await repository.participarSorteio(usuario, sorteio);
      utilService.fecharAlert();
      utilService.snackBar(
          titulo: 'Parabéns!',
          mensagem: 'Você está participando do sorteio! Agora é só aguardar');
    } catch (e) {
      print('Erro: $e');
      utilService.fecharAlert();
      utilService.snackBarErro();
    }
  }

  alertInstrucoes(Sorteio sorteio, bool recorrente) async {
    var result = await Get.dialog(AlertDialog(
      title: Text('Atenção!'),
      content: Text(
          'Para participar do sorteio, é preciso seguir todas as regras no post oficial do Instagram. Clique no botão abaixo para acessar.'),
      actions: [
        TextButton.icon(
            onPressed: () async {
              await utilService.lauchUrl(sorteio.link);
              Get.back(result: true);
            },
            icon: Icon(
              Zocial.instagram,
              color: Colors.pink[700],
            ),
            label: Text(
              'POST OFICIAL',
              style: TextStyle(
                color: Colors.pink[700],
              ),
            ))
      ],
    ));
    await Future.delayed(Duration(seconds: 1));
    await Get.dialog(AlertDialog(
      title: Text('Parabéns!'),
      content: Text(
          'Se voce cumpriu todos os passos, já está concorrendo ao nosso sorteio.'),
      actions: [
        TextButton.icon(
            onPressed: () async {
              Get.back(result: true);
            },
            icon: Icon(
              Icons.check,
              color: Colors.pink[700],
            ),
            label: Text(
              'CONCLUIR',
              style: TextStyle(
                color: Colors.pink[700],
              ),
            ))
      ],
    ));
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/custom_widgets/sorteio_card.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';

class PendentesTab extends StatelessWidget {
  final SorteiosController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetX<SorteiosController>(
        builder: (_) {
          if (_.sorteiosPendentes.isEmpty) {
            return Container(
                margin: EdgeInsets.only(top: 30),
                child: Text(
                    "Não há sorteios ativos no momento.\nMas aguarde, em breve novidades!",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20)));
          }
          return ListView.builder(
              itemCount: _.sorteiosPendentes.length,
              itemBuilder: (context, index) {
                Sorteio sorteio = _.sorteiosPendentes[index];
                return SorteioCard(sorteio);
              });
        },
      ),
    );
  }
}

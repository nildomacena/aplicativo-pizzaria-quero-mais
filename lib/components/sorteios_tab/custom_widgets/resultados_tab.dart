import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/custom_widgets/sorteio_card.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';

class ResultadosTab extends StatelessWidget {
  final SorteiosController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetX<SorteiosController>(
        builder: (_) {
          return ListView.builder(
              itemCount: _.resultadoSorteios.length,
              itemBuilder: (context, index) {
                Sorteio sorteio = _.resultadoSorteios[index];
                return SorteioCard(sorteio);
              });
        },
      ),
    );
  }
}

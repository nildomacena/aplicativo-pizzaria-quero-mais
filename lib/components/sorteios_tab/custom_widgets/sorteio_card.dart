import 'package:extended_image/extended_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class SorteioCard extends StatelessWidget {
  final Sorteio sorteio;
  final SorteiosController controller = Get.find();
  SorteioCard(this.sorteio);
  @override
  Widget build(BuildContext context) {
    bool jaParticipante =
        sorteio.participantes.contains(controller.usuario?.uid);
    return Card(
      margin: EdgeInsets.fromLTRB(10, 15, 10, 25),
      child: Column(
        children: [
          ExtendedImage.network(
            sorteio.imagem ??
                'https://st3.depositphotos.com/1518767/15994/i/1600/depositphotos_159945820-stock-photo-hamburger-with-french-fries.jpg',
            fit: BoxFit.cover,
          ),
          /* Container(
            width: double.infinity,
            padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
            child: Text(sorteio.titulo,
                style: TextStyle(
                    color: Get.theme.primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 22)),
          ), */
          Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.fromLTRB(20, 20, 20, 10),
                  child: Text(sorteio.titulo,
                      style: TextStyle(
                          color: Get.theme.primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22)),
                ),
              ),
              if (sorteio.pendente)
                Expanded(
                  child: ElevatedButton(
                      child: Text(jaParticipante ? 'Já inscrito' : 'Participar',
                          style: TextStyle(color: Colors.white)),
                      style: ElevatedButton.styleFrom(
                        primary: Get.theme.primaryColor,
                      ),
                      onPressed: jaParticipante
                          ? null
                          : () {
                              controller.onParticiparSorteio(sorteio);
                            }),
                ),
              Padding(
                padding: EdgeInsets.all(7),
              )
            ],
          ),
          if (sorteio.pendente)
            Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(20, 0, 5, 10),
              child: Text(
                sorteio.descricao,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w300),
              ),
            ),
          Container(
            height: 55,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    //color: Colors.blue,
                    alignment: Alignment.bottomLeft,
                    margin: EdgeInsets.fromLTRB(20, 0, 5, 15),
                    child: Text(
                        'Data do sorteio: ${sorteio.data.day}/${sorteio.data.month}/${sorteio.data.year}',
                        style: TextStyle(
                            color: Get.theme.primaryColor, fontSize: 18))),
                /* if (sorteio.pendente)
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: ElevatedButton(
                          child: Text(
                              jaParticipante ? 'Já inscrito' : 'Participar',
                              style: TextStyle(color: Colors.white)),
                          style: ElevatedButton.styleFrom(
                            primary: Get.theme.primaryColor,
                          ),
                          onPressed: jaParticipante
                              ? null
                              : () {
                                  controller.onParticiparSorteio(sorteio);
                                }),
                    ),
                  ), */
              ],
            ),
          ),
          if (!sorteio.pendente) ...{
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(20, 0, 5, 10),
              child: Text(
                  controller.usuario.uid == sorteio.ganhadorUid
                      ? 'PARABÉNS, VOCÊ FOI O GANHADOR!!'
                      : 'Nome do ganhador: ${sorteio.ganhador}',
                  style: TextStyle(
                      color: Get.theme.primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18)),
            )
          },
        ],
      ),
    );
  }
}

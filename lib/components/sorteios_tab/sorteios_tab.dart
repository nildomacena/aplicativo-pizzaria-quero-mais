import 'package:flutter/material.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/custom_widgets/pendentes_tab.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/custom_widgets/resultados_tab.dart';

class SorteiosTab extends StatefulWidget {
  @override
  _SorteiosTabState createState() => _SorteiosTabState();
}

class _SorteiosTabState extends State<SorteiosTab>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 2);
    /* if(authService.currentUser != null){
      print('user: ${authService.currentUser}');
      Fluttertoast.showToast(
        msg: "Você está logado com o email: ${authService.currentUser.email}",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );

    } */
    super.initState();
  }

  Widget build(BuildContext context) {
    /* fireService.getSorteiosPendentes().listen((sorteios) {
      print(sorteios);
    }); */
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          color: Theme.of(context).primaryColor,
          child: SafeArea(
            child: TabBar(
              indicatorWeight: 3,
              indicatorColor: Colors.white,
              controller: _tabController,
              tabs: <Widget>[
                Tab(
                  child: Text(
                    "Sorteios",
                  ),
                ),
                Tab(child: Text("Resultados")),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        //padding: EdgeInsets.all(10),
        child: TabBarView(
          controller: _tabController,
          children: <Widget>[
            PendentesTab(),
            ResultadosTab(),
          ],
        ),
      ),
    );
  }
}

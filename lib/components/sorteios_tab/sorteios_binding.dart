import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_controller.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_repository.dart';

class SorteiosBinding implements Bindings {
  @override
  void dependencies() {
    print('Sorteios Binding');
    Get.lazyPut<SorteiosRepository>(() => SorteiosRepository());
    Get.lazyPut<SorteiosController>(() => SorteiosController());
  }
}

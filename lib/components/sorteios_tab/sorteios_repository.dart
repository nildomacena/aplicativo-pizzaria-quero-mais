import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class SorteiosRepository {
  final FirestoreProvider firestoreProvider = Get.find();
  final AuthProvider authProvider = Get.find();

  Stream<List<Sorteio>> resultadosSorteios() {
    return firestoreProvider.streamResultadosSorteios();
  }

  Stream<List<Sorteio>> streamSorteiosPendentes() {
    return firestoreProvider.streamSorteiosPendentes();
  }

  Stream<Usuario> streamUsuario() {
    return authProvider.usuario$;
  }

  Future<void> participarSorteio(Usuario usuario, Sorteio sorteio) {
    return firestoreProvider.participarSorteio(usuario, sorteio);
  }

  Future<void> fazerLogin() {
    return authProvider.loginComGoogle();
  }
}

import 'dart:async';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:location/location.dart';

class SelecionaLocalizacaoController extends GetxController {
  LocationData localizacaoSelecionada;
  LocationData localizacaoInicial;
  Marker marker;
  Completer<GoogleMapController> completerGoogleMap = Completer();
  CameraPosition cameraPosition;
  SelecionaLocalizacaoController() {
    print('Get.arguments: ${Get.arguments}');
    if (Get.arguments != null && Get.arguments['localizacaoInicial'] != null)
      localizacaoSelecionada =
          localizacaoInicial = Get.arguments['localizacaoInicial'];
    marker = Marker(
        markerId: MarkerId('marker'),
        //infoWindow: InfoWindow(title: 'marker_id_1', snippet: '*'),
        onDragEnd: (LatLng latlng) {
          print('$latlng');
        },
        draggable: true,
        flat: true,
        position: LatLng(
          localizacaoInicial?.latitude ?? utilService.latitude,
          localizacaoInicial?.longitude ?? utilService.latitude,
        ));

    cameraPosition = CameraPosition(
        //bearing: 192.8334901395799,
        target:
            LatLng(localizacaoInicial.latitude, localizacaoInicial.longitude),
        //tilt: 59.440717697143555,
        zoom: 17);
  }

  selecionarLocalizacao() async {
    if (localizacaoSelecionada == null && localizacaoInicial != null) {
      Get.back(result: {
        'localizacaoSelecionada': localizacaoInicial,
        'enderecoLocation':
            await utilService.locationToAddress(localizacaoSelecionada)
      });
      return;
    }
    Get.back(result: {
      'localizacaoSelecionada': localizacaoSelecionada,
      'enderecoLocation':
          await utilService.locationToAddress(localizacaoSelecionada)
    });
  }

  setMarker(LatLng latLng) {
    marker = Marker(
        markerId: MarkerId('marker'),
        //infoWindow: InfoWindow(title: 'marker_id_1', snippet: '*'),
        onDragEnd: setMarker,
        draggable: true,
        position: latLng);
    localizacaoSelecionada = LocationData.fromMap(
        {'latitude': latLng.latitude, 'longitude': latLng.longitude});
    update();
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:app_pizzaria_quero_mais/components/seleciona_localizacao/seleciona_localizacao_controller.dart';

class SelecionaLocalizacaoPage extends StatelessWidget {
  final SelecionaLocalizacaoController controller =
      Get.put(SelecionaLocalizacaoController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: Get.width,
          height: Get.height,
          child: Stack(
            fit: StackFit.expand,
            children: [
              Container(
                  width: Get.width,
                  height: Get.height,
                  child: GetBuilder<SelecionaLocalizacaoController>(
                    builder: (_) {
                      return GoogleMap(
                        buildingsEnabled: false,
                        myLocationEnabled: true,
                        compassEnabled: false,
                        myLocationButtonEnabled: true,
                        tiltGesturesEnabled: false,
                        indoorViewEnabled: false,
                        //liteModeEnabled: true,
                        mapToolbarEnabled: false,
                        onTap: controller.setMarker,
                        markers: Set<Marker>.of([controller.marker]),
                        mapType: MapType.terrain,
                        initialCameraPosition: controller.cameraPosition,
                        onMapCreated: (GoogleMapController _controller) {
                          try {
                            controller.completerGoogleMap.complete(_controller);
                          } catch (e) {
                            print('Erro ao criar mapa');
                          }
                        },
                      );
                    },
                  )),
              Positioned(
                  bottom: 20,
                  right: (Get.width * .5) - 115,
                  child: Container(
                    width: 230,
                    height: 60,
                    child: GetBuilder<SelecionaLocalizacaoController>(
                      builder: (_) {
                        if (controller.localizacaoSelecionada == null)
                          return Container();
                        return TextButton(
                          style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              backgroundColor: Get.theme.primaryColor,
                              primary: Colors.white),
                          onPressed: controller.selecionarLocalizacao,
                          child: Text(
                            'Selecionar localização',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        );
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

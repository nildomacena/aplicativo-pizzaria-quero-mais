import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_controller.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_repository.dart';

class CategoriaBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CategoriaRepository>(() => CategoriaRepository());
    Get.lazyPut<CategoriaController>(() => CategoriaController());
  }
}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';

class TileItemCardapio extends StatelessWidget {
  final Item item;
  final CategoriaController controller;
  final bool lastItem;
  const TileItemCardapio(this.item, this.controller, this.lastItem);

  Widget imagem() {
    return Container(
      alignment: Alignment.center,
      width: Get.width * .30,
      height: 120,
      child: Stack(
        fit: StackFit.expand,
        children: [
          ExtendedImage.network(
            item.imagemUrl,
            fit: BoxFit.fill,
          ),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {
                  //controller.goToItem(item);
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10, bottom: lastItem ? 100 : 10),
      child: Card(
        child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            height: 120,
            //color: Colors.blue,
            child: Row(
              children: [
                imagem(),
                Expanded(
                  child: InkWell(
                    onTap: () {
                      controller.showModal(context, item);
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 10, left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${item.nome}',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w500),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: AutoSizeText(
                              item.descricao,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              'R\$${item.preco.toStringAsFixed(2)}',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(
                              'Cashback: R\$${(item.preco * controller.cashback).toStringAsFixed(2)}',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  width: 40,
                  height: double.infinity,
                  child: Material(
                    child: Ink(
                      color: Colors.red,
                      child: InkWell(
                        splashColor: Colors.white,
                        child: Icon(
                          MaterialIcons.add_shopping_cart,
                          color: Colors.white,
                        ),
                        onTap: () {
                          controller.showModal(context, item);
                        },
                      ),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }
}

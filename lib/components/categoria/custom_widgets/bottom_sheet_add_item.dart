import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';

class BottomSheetAddItem extends StatelessWidget {
  final Item item;
  final CategoriaController controller;

  BottomSheetAddItem(this.item, this.controller);

  @override
  Widget build(BuildContext context) {
    Widget rowQuantidade() {
      return GetBuilder<CategoriaController>(builder: (_) {
        return Container(
          width: double.infinity,
          height: 100,
          margin: EdgeInsets.only(top: 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AnimatedOpacity(
                opacity: controller.qtdAdicionar > 1 ? 1 : 0,
                duration: Duration(milliseconds: 700),
                child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(30),
                      onTap: controller.qtdAdicionar <= 1
                          ? null
                          : () {
                              print('Diminuir quantidade');
                              if (controller.qtdAdicionar > 1)
                                controller.subQuantidade();
                            },
                      child: Container(
                        height: 45,
                        width: 45,
                        decoration: BoxDecoration(
                            color: controller.qtdAdicionar == 1
                                ? Colors.grey[300]
                                : Colors.white,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(color: Colors.grey)),
                        child: Icon(
                          Icons.remove,
                          color: controller.qtdAdicionar == 1
                              ? Colors.grey
                              : Colors.black,
                        ),
                      ),
                    )),
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15),
                child: Text(
                  controller.qtdAdicionar.toString(),
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w300),
                ),
              ),
              Material(
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {
                      controller.addQuantidade();

                      print('Aumentar quantidade');
                    },
                    child: Container(
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          border: Border.all(color: Colors.grey)),
                      child: Icon(Icons.add),
                    ),
                  )),
            ],
          ),
        );
      });
    }

    /*   Widget checkMeiaPizza() {
      return GetBuilder<CategoriaController>(builder: (_) {
        return Container(
          /* height: 100,
          width: Get.width, */
          child: Column(
            children: [
              Container(
                child: Text(
                  'Selecione meia pizza se deseja 2 sabores na pizza',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: RadioListTile(
                        title: AutoSizeText(
                          'Pizza Inteira',
                          maxLines: 1,
                        ),
                        value: 'inteira',
                        groupValue: _.tipoPizza,
                        onChanged: _.toggleTipoPizza),
                  ),
                  Expanded(
                    child: RadioListTile(
                        title: AutoSizeText(
                          'Meia Pizza',
                          maxLines: 1,
                        ),
                        value: 'meia',
                        groupValue: _.tipoPizza,
                        onChanged: _.toggleTipoPizza),
                  ),
                ],
              ),
            ],
          ),
        );
        return CheckboxListTile(
            value: _.meiaPizza, onChanged: _.toggleMeiaPizza);
      });
    } */

    /*  Widget buttonMontarPizza() {
      return Container(
        margin: EdgeInsets.only(top: 40),
        child: TextButton(
          style: TextButton.styleFrom(
              minimumSize: Size(250, 60),
              backgroundColor: Colors.black,
              shadowColor: Colors.white),
          child: Text(controller.textoBotaoBottomSheet),
          /* Text(
            'Adicionar ${controller.qtdAdicionar} ao Carrinho - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}',
            style: TextStyle(
                fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold),
          ), */
          onPressed: () {
            controller.addEntrada(item);
          },
        ),
      );
    }
 */
    Widget buttonSalvar() {
      return GetBuilder<CategoriaController>(
        builder: (_) {
          print('item is pizza ${item.isPizza}');
          /* if (item.isPizza)
            return Row(
              children: [
                TextButton(
                  style: TextButton.styleFrom(
                      minimumSize: Size(250, 60),
                      backgroundColor: Colors.black,
                      shadowColor: Colors.white),
                  child: Text(
                    'Adicionar ${controller.qtdAdicionar} ao Carrinhoss - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    controller.addEntrada(item);
                  },
                ),
                TextButton(
                  style: TextButton.styleFrom(
                      minimumSize: Size(250, 60),
                      backgroundColor: Colors.black,
                      shadowColor: Colors.white),
                  child: Text(
                    'Adicionar ${controller.qtdAdicionar} ao Carrinhoss - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    controller.addEntrada(item);
                  },
                ),
              ],
            ); */
          return Container(
            margin: EdgeInsets.only(top: 40),
            child: TextButton(
              style: TextButton.styleFrom(
                  minimumSize: Size(250, 60),
                  backgroundColor: Colors.black,
                  shadowColor: Colors.white),
              child: Text(
                controller.textoBotaoBottomSheet,
                /* 'Adicionar ${controller.qtdAdicionar} ao Carrinho - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}', */
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                controller.addEntrada(item);
              },
            ),
          );
          /*  return Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: TextButton(
              style: TextButton.styleFrom(minimumSize: Size(250, 50)),
              onPressed: () {
                controller.addEntrada(item);
              },
              child: Text(
                'Adicionar ${controller.qtdAdicionar} ao Carrinho - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ); */
        },
      );
    }

    return Container(
      padding: EdgeInsets.only(top: 10, right: 10, left: 10, bottom: 30),
      color: Colors.white,
      //height: 350,
      child: ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        /* mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center, */
        children: [
          Text(
            '${item.nome}',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          Divider(),
          if (item.isPizza)
            Container(
              child: Text(
                'Ingredientes: ${item.descricao}',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
              ),
            ),
          //if (item.isPizza) checkMeiaPizza(),
          if (!item.isPizza)
            Container(
              margin: EdgeInsets.only(top: 10),
              child: TextField(
                controller: controller.observacoesController,
                maxLines: 3,
                textInputAction: TextInputAction.done,
                decoration: InputDecoration(
                  hintText: 'Observações: Ex. Sem presunto',
                  focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Get.theme.primaryColor, width: 1.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: Get.theme.primaryColor, width: 1.0),
                  ),
                ),
              ),
            ),
          if (controller.cashback > 0)
            Container(
              margin: EdgeInsets.only(top: 15),
              child: Text(
                'Esse item rende um desconto de R\$${(item.preco * controller.cashback).toStringAsFixed(2)} na próxima compra',
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.w300),
              ),
            ),
          if (!item.isPizza) rowQuantidade(),
          buttonSalvar()
        ],
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class CategoriaRepository {
  final FirestoreProvider firestoreProvider = Get.find();

  Future<List<Item>> getItens(Categoria categoria) {
    return firestoreProvider.getItensPorCategoria(categoria);
  }

  Future<double> get cashback => firestoreProvider.getCashback();
}

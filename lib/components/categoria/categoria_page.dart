import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_controller.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/custom_widgets/tile_item.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class CategoriaPage extends StatelessWidget {
  final CategoriaController controller = Get.find();

  Widget slidingUp() {
    return GetX<CategoriaController>(
      builder: (_) {
        /* if (_.currentPizza == null || _.currentPizza.sabores.length == 0)
          return Container(); */
        return SlidingUpPanel(
            controller: controller.panelController,
            minHeight: 60,
            panel: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  onDoubleTap: () async {
                    print('double tap');
                    controller.panelController.open();
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 10),
                    height: 5,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                ),
                Container(
                  height: 50,
                  width: Get.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          margin: EdgeInsets.only(left: 10),
                          child: AutoSizeText(
                            controller.pizzasProvider.doisSabores
                                ? 'Você está escolhendo dois sabores'
                                : 'Você está escolhendo um sabor',
                            maxLines: 1,
                            style: TextStyle(fontSize: 15),
                          )),
                      TextButton(
                          onPressed: controller.mudarQuantidadeSabores,
                          child: Text('ALTERAR'))
                    ],
                  ),
                ),
                Divider(),
                if (controller.pizzasProvider.currentPizza.sabores.isNotEmpty &&
                    controller.pizzasProvider.doisSabores)
                  ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: controller.pizzasProvider.currentPizza.sabores
                        .map((e) => Container(
                              margin: EdgeInsets.only(
                                  left: 10, bottom: 5, right: 10),
                              child: Row(
                                children: [
                                  Text(
                                    '1/2 ${e.nome}  ',
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  Expanded(
                                      child: Container(
                                    child: AutoSizeText(
                                      '-----------------------------------------------------',
                                      maxLines: 1,
                                    ),
                                  )),
                                  Container(
                                    margin: EdgeInsets.only(),
                                    child: Text(
                                      'R\$${e.preco.toStringAsFixed(2)}',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                  Container(
                                    child: IconButton(
                                      onPressed: () {
                                        controller.deleteSaborSelecionado(e);
                                      },
                                      icon: Icon(
                                        Icons.close,
                                      ),
                                      iconSize: 30,
                                      color: Colors.red,
                                    ),
                                  )
                                ],
                              ),
                            ))
                        .toList(),
                  ),
                if (controller.pizzasProvider.currentPizza.sabores.isNotEmpty &&
                    !controller.pizzasProvider.doisSabores)
                  Container(
                    margin: EdgeInsets.only(left: 10, bottom: 5, right: 10),
                    child: Row(
                      children: [
                        Text(
                          '1 ${controller.pizzasProvider.currentPizza.sabores.first.nome}  ',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.w300),
                        ),
                        Expanded(
                            child: Container(
                          child: AutoSizeText(
                            '-----------------------------------------------------',
                            maxLines: 1,
                          ),
                        )),
                        Container(
                          margin: EdgeInsets.only(),
                          child: Text(
                            'R\$${controller.pizzasProvider.currentPizza.preco.toStringAsFixed(2)}',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.w300),
                          ),
                        ),
                        Container(
                          child: IconButton(
                            onPressed: () {
                              controller.deleteSaborSelecionado(controller
                                  .pizzasProvider.currentPizza.sabores.first);
                            },
                            icon: Icon(
                              Icons.close,
                            ),
                            iconSize: 30,
                            color: Colors.red,
                          ),
                        )
                      ],
                    ),
                  ),
                if (controller.cashback > 0) ...{
                  Divider(),
                  Container(
                    margin: EdgeInsets.only(top: 0, left: 10, right: 10),
                    child: Text(
                      'Comprando esse item você tem R\$${(controller.pizzasProvider.currentPizza.preco * controller.cashback).toStringAsFixed(2)} de desconto na próxima compra',
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
                    ),
                  )
                },
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: TextButton(
                    style: TextButton.styleFrom(
                        minimumSize: Size(250, 60),
                        backgroundColor: controller.podeAdicionarPizza
                            ? Get.theme.primaryColor
                            : Colors.grey,
                        shadowColor: Colors.white),
                    child: Text(
                      controller.textoBotaoAdicionarPizza,
                      /* 'Adicionar ${controller.qtdAdicionar} ao Carrinho - R\$${(controller.qtdAdicionar * item.preco).toStringAsFixed(2)}', */
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: controller.podeAdicionarPizza
                        ? () {
                            controller.addPizzaAoCarrinho();
                          }
                        : null,
                  ),
                )
              ],
            ));
      },
    );
  }

  Widget cardInfo() {
    return Card(
      child: Container(
        height: 80,
        width: Get.width,
        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Text(
                  controller.pizzasProvider.doisSabores
                      ? 'Você está escolhendo dois sabores'
                      : 'Você está escolhendo um sabor',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                )),
            Obx(() => Container(
                  height: 30,
                  child: ElevatedButton(
                      onPressed: controller.mudarQuantidadeSabores,
                      child: Text(
                        controller.doisSabores
                            ? 'Mudar para um sabor'.toUpperCase()
                            : 'Mudar para dois sabores'.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 12),
                      )),
                )),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controller.categoria?.nome ?? ''),
      ),
      body: Container(
        child: GetBuilder<CategoriaController>(
          builder: (_) {
            if (controller.itensCardapio == null)
              return Center(
                child: CircularProgressIndicator(),
              );
            else
              return Stack(
                //fit: StackFit.expand,
                children: [
                  Container(
                    //margin: EdgeInsets.only(top: 80),
                    child: ListView.builder(
                        itemCount: controller.itensCardapio?.length,
                        itemBuilder: (context, index) {
                          Item item = controller.itensCardapio[index];
                          return TileItemCardapio(item, controller,
                              index == controller.itensCardapio.length - 1);
                        }),
                  ),
                  Positioned(child: slidingUp()),
                ],
              );
          },
        ),
      ),
    );
  }
}

import 'package:app_pizzaria_quero_mais/data/model/pizza.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/pizzas_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_repository.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/custom_widgets/bottom_sheet_add_item.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class CategoriaController extends GetxController {
  final PanelController panelController = PanelController();
  final PizzasProvider pizzasProvider = Get.find();
  final CategoriaRepository repository = Get.find();
  final CarrinhoController carrinhoController = Get.find();
  double cashback = 0;

  RxBool _doisSabores = false.obs;
  Rx<Pizza> _currentPizza = Pizza().obs;
  RxList<Item> _sabores = <Item>[].obs;

  Categoria categoria;
  List<Item> itensCardapio;
  TextEditingController observacoesController = TextEditingController();
  int qtdAdicionar = 1;
  bool meiaPizza = false;
  String tipoPizza = 'inteira';

  Pizza get currentPizza => _currentPizza.value;
  List<Item> get sabores => _sabores.toList();

  bool get doisSabores => _doisSabores.value;
  String get textoBotaoBottomSheet => pizzasProvider.doisSabores
      ? pizzasProvider.currentPizza.sabores.isEmpty
          ? 'Selecionar primeiro sabor'
          : 'Selecionar segundo sabor'
      : 'Adicionar pizza ao carrinho';

  bool get podeAdicionarPizza =>
      (pizzasProvider.doisSabores &&
          pizzasProvider.currentPizza.sabores.length == 2) ||
      (!pizzasProvider.doisSabores &&
          pizzasProvider.currentPizza.sabores.isNotEmpty);

  String get textoBotaoAdicionarPizza => (pizzasProvider.doisSabores &&
              pizzasProvider.currentPizza.sabores.length == 2) ||
          (!pizzasProvider.doisSabores &&
              pizzasProvider.currentPizza.sabores.isNotEmpty)
      ? 'Adicionar pizza ao carrinho - R\$${pizzasProvider.currentPizza.preco.toStringAsFixed(2)}'
      : pizzasProvider.doisSabores &&
              pizzasProvider.currentPizza.sabores.isEmpty
          ? 'Selecione o primeiro sabor'
          : pizzasProvider.doisSabores
              ? 'Selecione o segundo sabor'
              : 'Selecione o sabor de pizza';

  CategoriaController() {
    _doisSabores.bindStream(pizzasProvider.doisSabores$);
    _currentPizza.bindStream(pizzasProvider.currentPizza$);
    categoria = Get.arguments['categoria'];
    if (categoria == null) Get.offAllNamed(Routes.HOME);
    initPromises();
    /* if (Get.arguments['categoria'] != null) {
      Get.offAllNamed(Routes.HOME);
    } else {
    } */
  }

  @override
  void onInit() async {
    /* await Future.delayed(Duration(seconds: 2));
    print('Get.arguments[categoria]: ${Get.arguments['categoria']}'); */

    super.onInit();
  }

  initPromises() async {
    //await Future.delayed(Duration(milliseconds: 500));
    itensCardapio = await repository.getItens(categoria);
    cashback = await repository.cashback;
    update();
  }

  mudarQuantidadeSabores() {
    pizzasProvider.mudarQuantidadeSabores();
  }

  showModal(BuildContext context, Item item) async {
    /* showMaterialModalBottomSheet(
      context: context,
      builder: (context) => SingleChildScrollView(
        controller: ModalScrollController.of(context),
        child: Container(
          height: 300,
          width: Get.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [Text('Adicionar ao carrinho')],
          ),
        ),
      ),
    ); */
    await Get.bottomSheet(BottomSheetAddItem(item, this));
    observacoesController.text = '';
    qtdAdicionar = 1;
    meiaPizza = false;
  }

  toggleMeiaPizza(bool value) {
    meiaPizza = value ?? !meiaPizza;
    update();
  }

  toggleTipoPizza(String tipo) {
    tipoPizza = tipo;
    update();
  }

  addQuantidade() {
    qtdAdicionar++;
    update();
  }

  subQuantidade() {
    if (qtdAdicionar == 1) return;
    qtdAdicionar--;
    update();
  }

  addEntrada(Item item) {
    if (item.isPizza && pizzasProvider.doisSabores) {
      adicionarSaborPizza(item);
      return;
    }
    Entrada entrada = Entrada(item, qtdAdicionar);
    if (Get.isBottomSheetOpen ?? false) Get.back();
    carrinhoController.addEntrada(entrada);
    Get.offAndToNamed(Routes.HOME);
    utilService.snackBarItemAdicionado(item);
  }

  animarPreviewPanel() async {
    panelController.animatePanelToPosition(.4,
        duration: Duration(milliseconds: 500));
    await Future.delayed(Duration(milliseconds: 1000));
    panelController.animatePanelToPosition(0,
        duration: Duration(seconds: 1), curve: Curves.bounceOut);
  }

  abrirPanel() {
    panelController.animatePanelToPosition(1,
        duration: Duration(milliseconds: 600));
  }

  fecharPanel() {
    panelController.animatePanelToPosition(0,
        duration: Duration(milliseconds: 600));
  }

  deleteSaborSelecionado(Item sabor) {
    try {
      pizzasProvider.deleteSabor(sabor);
      if (pizzasProvider.currentPizza.sabores.isEmpty) fecharPanel();
      update();
    } catch (e) {
      print('Ocorreu um erro: $e');
    }
  }

  adicionarSaborPizza(Item item) async {
    try {
      if (Get.isBottomSheetOpen) Get.back();
      pizzasProvider.adicionarSaborPizza(item);
      if (pizzasProvider.currentPizza.sabores.length < 2) animarPreviewPanel();
      if (pizzasProvider.currentPizza.sabores.length == 2) abrirPanel();
      utilService.snackBar(titulo: 'Pronto', mensagem: 'Sabor adicionado');
    } catch (e) {
      print('Erro: $e');
      if (e.contains('sabor-ja-escolhido')) {
        /*  panelController.animatePanelToPosition(1,
            duration: Duration(milliseconds: 600)); */
        utilService.snackBarErro(mensagem: 'Você já escolheu esse sabor');
      }
      if (e.contains('num-sabores-excedido')) {
        panelController.animatePanelToPosition(1,
            duration: Duration(milliseconds: 600));
        utilService.snackBarErro(mensagem: 'Voce já escolheu dois sabores');
      }
    }
  }

  goToItem(Item item) {
    Get.toNamed(Routes.ITEM, arguments: {'item': item});
  }

  addPizzaAoCarrinho() {
    pizzasProvider.addPizzaAoCarrinho();
    Get.back();
    if (Get.isSnackbarOpen) Get.back();
    Get.offAndToNamed(Routes.HOME);
    utilService.snackBar(
        titulo: 'Sucesso', mensagem: 'Pizza adicionada ao carrinho');
  }
}

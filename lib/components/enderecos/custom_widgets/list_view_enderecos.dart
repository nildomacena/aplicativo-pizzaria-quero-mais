import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';

class ListViewEnderecos extends StatelessWidget {
  Widget placeholderCarregando() {
    return Container(
      height: 300,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.grey),
              backgroundColor: Get.theme.primaryColor,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 30),
            child: Text(
              'Carregando dados...',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
            ),
          ),
        ],
      ),
    );
  }

  Widget listaVazia() {
    return Container(
      //height: 300,
      margin: EdgeInsets.only(top: 30),
      alignment: Alignment.center,
      child: Text(
          'Você não possui endereços cadastrados\nClique no botão acima para adicionar',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetBuilder<EnderecosController>(
        builder: (controller) {
          if (controller.enderecos == null) return placeholderCarregando();
          if (controller.enderecos.isEmpty) return listaVazia();
          return ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: controller.enderecos.length,
              itemBuilder: (context, index) {
                Endereco endereco = controller.enderecos[index];
                return Container(
                  margin:
                      EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
                  height: 110,
                  width: MediaQuery.of(context).size.width,
                  //padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                      //color: Colors.grey[300],
                      border: Border.all(color: Get.theme.primaryColor),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      InkWell(
                        splashColor: Colors.black12,
                        onLongPress: () {
                          controller.showBottomSheet(endereco);
                        },
                        onTap: () {
                          controller.onSelectEndereco(endereco);
                          /*  if (widget.veioDoCarrinho)
                            Navigator.pop(context, endereco);
                          print('Ir para ${endereco.nome}'); */
                        },
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '${endereco.logradouro}, ${endereco.numero} ',
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              AutoSizeText(
                                '${endereco.bairro}',
                                maxLines: 2,
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              /*  Text(
                                '${endereco.bairro}',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w300),
                              ), */
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        top: 10,
                        right: 0,
                        child: Column(
                          children: <Widget>[
                            /* if (endereco.padrao)
                              Icon(Icons.check_circle,
                                  color: Get.theme.primaryColor, size: 17), */
                            IconButton(
                                icon: Icon(Icons.more_vert),
                                onPressed: () async {
                                  controller.showBottomSheet(endereco);
                                  /*  print('More: ${endereco.nome}');
                                  bool excluiu =
                                      await _enderecoModalBottomSheet(context,
                                              endereco, authProvider.usuario) ??
                                          false;
                                  if (excluiu) setState(() {});
                                  Future.delayed(Duration(milliseconds: 500));
                                  _searchFocus.unfocus(); */
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}

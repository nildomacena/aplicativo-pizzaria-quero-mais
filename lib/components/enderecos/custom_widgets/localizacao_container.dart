import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_controller.dart';

class LocalizacaoContainer extends StatelessWidget {
  final EnderecosController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EnderecosController>(builder: (_) {
      return Material(
        elevation: 5,
        child: Container(
          child: Column(
            children: <Widget>[
              /* Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  style: TextStyle(fontSize: 18),
                  focusNode: controller.searchFocus,
                  autofocus: false,
                  controller: controller.searchController,
                  onTap: controller.showPlacesAutoComplete,
                  onChanged: controller.showPlacesAutoComplete,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: controller.clearSearchText),
                      fillColor: Colors.grey[200],
                      hintText: 'Digite endereço e número',
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.grey,
                        ),
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                ),
              ), */
              Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width,
                  child: InkWell(
                    onTap: controller.handleClickContainer,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.location_on,
                          color: Colors.blueGrey[300],
                          size: 40,
                        ),
                        Expanded(
                            child: Container(
                          margin: EdgeInsets.only(top: 25, left: 5, bottom: 10),
                          height: double.infinity,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Cadastrar novo endereço',
                                style: TextStyle(fontSize: 20),
                              ),
                              Text(
                                '${controller.carregandoLocalizacao && controller.enderecoLocation == null ? 'Carregando localização...' : controller.enderecoLocation != null ? controller.enderecoLocation : 'Ativar GPS'}',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ))
                      ],
                    ),
                  ))
            ],
          ),
        ),
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/custom_widgets/list_view_enderecos.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/custom_widgets/localizacao_container.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_controller.dart';

class EnderecosPage extends StatelessWidget {
  EnderecosController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Get.theme.primaryColor,
        child: Icon(Icons.add),
        onPressed: controller.goToNovoEndereco,
      ),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Endereços'),
      ),
      body: Container(
        child: ListView(
          children: [/* LocalizacaoContainer(), */ ListViewEnderecos()],
        ),
      ),
    );
  }
}

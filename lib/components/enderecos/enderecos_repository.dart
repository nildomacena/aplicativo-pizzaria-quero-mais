import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class EnderecosRepository {
  final AuthProvider authProvider = Get.find();
  final FirestoreProvider firestoreProvider = Get.find();

  Future<List<Endereco>> getEnderecos() {
    return authProvider.getEnderecosByUser();
  }

  Future<List<Endereco>> excluirEndereco(Endereco endereco) {
    return authProvider.excluirEndereco(endereco);
  }

  Future<List<Endereco>> tornarEnderecoPadrao(Endereco endereco) {
    return authProvider.tornarEnderecoPadrao(endereco);
  }
}

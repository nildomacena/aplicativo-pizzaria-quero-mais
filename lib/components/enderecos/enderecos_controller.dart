import 'package:geocoder/geocoder.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_repository.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:location/location.dart';

class EnderecosController extends GetxController {
  final EnderecosRepository repository = Get.find();
  TextEditingController searchController = TextEditingController();
  FocusNode searchFocus = FocusNode();
  Location location = Location();
  LocationData locationData;
  String enderecoLocation;
  Address address;
  bool carregandoLocalizacao = true;
  List<Endereco> enderecos;
  bool veioDoCarrinho = false;

  final _obj = ''.obs;
  set obj(value) => this._obj.value = value;
  get obj => this._obj.value;

  EnderecosController() {
    if (Get.arguments != null)
      veioDoCarrinho = Get.arguments['veioDoCarrinho'] ?? false;
    /* getFutureLocation();
    checkInitialLocation(); */
    initEnderecos();
  }

  initEnderecos() async {
    enderecos = await repository.getEnderecos();
    update();
  }

  Future<bool> checkInitialLocation() async {
    //Fecha algum alert que pode estar aberto
    utilService.fecharAlert();
    //Verifica se o aplicativo possui permissao para acessar o gps
    PermissionStatus _permissionGranted = await location.hasPermission();
    if (_permissionGranted != PermissionStatus.granted &&
        _permissionGranted != PermissionStatus.grantedLimited) {
      //Se nao possui, solicita permissao
      _permissionGranted = await location.requestPermission();

      //Se ainda assim o usuario nao permite, exibe alerta
      if (_permissionGranted != PermissionStatus.granted &&
          _permissionGranted != PermissionStatus.grantedLimited) {
        await utilService.showAlert('Permita acesso ',
            'Para salvar a seu endereço, o aplicativo precisa acessar sua localização',
            actionLabel: 'TENTAR NOVAMENTE', action: checkInitialLocation);
        return false;
      }
    }
    //Se tem permissao, verifica se o gps está ativo
    bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        await utilService.showAlert('Ligue o GPS',
            'Para salvar a seu endereço, o aplicativo precisa acessar sua localização',
            actionLabel: 'TENTAR NOVAMENTE', action: checkInitialLocation);
      }
    }
    return serviceEnabled;
  }

  showBottomSheet(Endereco endereco) {
    bool alterou = false;
    Get.bottomSheet(Container(
      color: Colors.white,
      child: Wrap(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 10),
              alignment: Alignment.center,
              child: Text(
                endereco.nome ?? endereco.logradouro,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              )),
          ListTile(
              leading: Icon(Icons.check_circle),
              title: Text('Tornar endereço padrão'),
              onTap: () async {
                utilService.showAlertCarregando('Aguarde...');
                enderecos = await repository.tornarEnderecoPadrao(endereco);
                utilService.fecharAlert();
                update();
                if (Get.isBottomSheetOpen ?? false) Get.back();
                /* await usuario.definirEnderecoPadrao(endereco);
                alterou = true;
                Navigator.pop(context, alterou); */
              }),
          ListTile(
            leading: Icon(Icons.delete),
            title: Text('Excluir Endereço'),
            onTap: () async {
              alterou = await Get.dialog(AlertDialog(
                title: Text('Confirmação'),
                content: Text('Deseja realmente excluir esse endereço?'),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text('Cancelar')),
                  TextButton(
                      onPressed: () async {
                        try {
                          Get.back();
                          utilService.showAlertCarregando('Aguarde...');
                          enderecos =
                              await repository.excluirEndereco(endereco);
                          utilService.fecharAlert();
                          if (Get.isBottomSheetOpen ?? false) Get.back();
                          update();
                        } catch (e) {
                          utilService.snackBarErro();
                          print('Ocorreu um erro durante a solicitação $e');
                        }
                      },
                      child: Text('Excluir'))
                ],
              ));
              if (alterou != null && alterou)
                Get.back(result: alterou ?? false);
            },
          ),
        ],
      ),
    ));
  }

  handleClickContainer() async {
    if (!(await location.serviceEnabled())) {
      await location.requestService();
      update();
      return;
    }
    if (address == null) {
      return;
    }
    var result = await Get.toNamed(Routes.NOVO_ENDERECO,
        arguments: {'enderecoGeocode': address, 'locationData': locationData});
    if (result != null && result['enderecos'] != null) {
      enderecos = result['enderecos'];
      update();
    }
  }

  Future<Address> getFutureLocation() async {
    print('getFutureLocation()');
    if (!(await requestLocationPermission())) {
      debugPrint('Nao autorizou');
      await location.requestPermission();
      return null; //Se o usuário não autorizar, já sai da funcao
    }
    if (await location.hasPermission() == PermissionStatus.granted &&
        await location.serviceEnabled()) {
      locationData = await location.getLocation();
      final coordinates =
          new Coordinates(locationData?.latitude, locationData?.longitude);
      List<Address> addresses =
          await Geocoder.local.findAddressesFromCoordinates(coordinates);
      address = addresses.first;
      String addressLine = address?.addressLine ?? '';
      if (addressLine.indexOf(' - AL, 5') > 0)
        enderecoLocation = address.addressLine
            .substring(0, address.addressLine.indexOf(' - AL, 5'));
      update();
      debugPrint('Address: ${address?.addressLine}');
      return address;
    } else {
      bool ativou = await location.requestService();
      carregandoLocalizacao = true;
      getFutureLocation();
      print('null $ativou');
      update();
      return null;
    }
  }

  Future<bool> requestLocationPermission() async {
    bool hasPermission =
        await location.hasPermission() == PermissionStatus.granted ||
            await location.hasPermission() == PermissionStatus.grantedLimited;
    //Se o usuário já permitiu o acesso, retorne true
    if (hasPermission) return hasPermission;
    //Senão, solicite permissão
    PermissionStatus permissionStatus = await location.requestPermission();
    if (permissionStatus == PermissionStatus.granted ||
        permissionStatus == PermissionStatus.grantedLimited) return true;
    return false;
  }

  clearSearchText() {
    searchController.text = '';
    update();
  }

  onSelectEndereco(Endereco endereco) {
    if (veioDoCarrinho) Get.back(result: {'endereco': endereco});
  }

  goToNovoEndereco() async {
    print('goToNovoEndereco');
    //Checa o GPS antes de avancar
    bool gpsOk = await checkInitialLocation();
    await getFutureLocation();
    print('gps ok: $gpsOk');
    if (!gpsOk) return;
    searchController.text = '';
    searchFocus.unfocus();
    var result = await Get.toNamed(Routes.NOVO_ENDERECO,
        arguments: {locationData: locationData});
    if (result != null && result['enderecos'] != null) {
      enderecos = result['enderecos'];
      update();
    }
    /* Endereco endereco = await Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => NovoEnderecoPage(address)));
    if (endereco != null) Navigator.pop(context, endereco); */
  }
}

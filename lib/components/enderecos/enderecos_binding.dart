import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_controller.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_repository.dart';

class EnderecosBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(EnderecosRepository());
    Get.lazyPut<EnderecosController>(() => EnderecosController());
  }
}

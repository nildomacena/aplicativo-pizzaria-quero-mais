import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_controller.dart';

class HomePage extends StatelessWidget {
  final HomeController controller = Get.find();
  final CarrinhoController carrinhoController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /* appBar: AppBar(
        title: Text('Home'),
      ), */
      bottomNavigationBar: GetBuilder<HomeController>(
        builder: (_) {
          return BottomNavigationBar(
            selectedItemColor: Get.theme.primaryColor,
            currentIndex: controller.indexTab,
            items: [
              BottomNavigationBarItem(
                icon: Icon(FontAwesome5Solid.hamburger),
                label: 'Cardápio',
              ),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesome.trophy), label: 'Sorteios'),
              /*  BottomNavigationBarItem(
            icon: Icon(MaterialCommunityIcons.book),
            title: Text('Pedidos'),
          ), */
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Perfil',
              ),
            ],
            onTap: controller.setTab,
          );
        },
      ),
      body: GetBuilder<HomeController>(
        builder: (_) => controller.tabs[controller.indexTab],
      ),
    );
  }
}

import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';
import 'package:get/get.dart';

class HomeRepository {
  final FirestoreProvider firestoreProvider = Get.find();

  HomeRepository();
  Future<bool> checkModoTeste() {
    return firestoreProvider.checkModoTeste();
  }
}

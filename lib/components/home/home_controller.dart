import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_tab.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_respository.dart';
import 'package:app_pizzaria_quero_mais/components/perfil_tab/perfil_tab.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_tab.dart';

class HomeController extends GetxController {
  final List<Widget> tabs = [
    CardapioTab(),
    SorteiosTab(),
    PerfilTab(),
  ];
  int indexTab = 0;
  final HomeRepository repository = Get.find();
  HomeController() {
    repository.checkModoTeste().then((modoTeste) {
      if (modoTeste != null && modoTeste == true) {
        utilService.showAlert('ATENÇÃO MODO TESTE',
            'Atenção, atualmente o aplicativo está em modo de teste! Utilize-o à vontade.\nTodos os dados serão resetados no ato do lançamento.');
      }
    });
  }

  setTab(int index) {
    indexTab = index;
    update();
  }
}

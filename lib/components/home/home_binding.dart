import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_repository.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_controller.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_respository.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(HomeRepository());
    Get.put(CardapioRepository());
    Get.put(CardapioController());
    Get.lazyPut<HomeController>(() => HomeController());
  }
}

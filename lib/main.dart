import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:app_pizzaria_quero_mais/bindings/initial_binding.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_page.dart';
import 'package:app_pizzaria_quero_mais/routes/app_pages.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background: ${message.data}");
  /* flutterLocalNotificationsPlugin.show(
      message.data.hashCode,
      message.notification.title,
      message.notification.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channel.description,
          enableVibration: true,
          playSound: true,
        ),
      )); */
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await GetStorage.init();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  FirebaseMessaging.instance.subscribeToTopic('notificacao');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Pizzaria Quero Mais',
      getPages: AppPages.routes,
      theme: ThemeData(
        primaryColor: Colors.red,
      ),
      initialBinding: InitialBinding(),
      initialRoute: Routes.SPLASHSCREEN,
      //home: Splashscreen(),
    );
  }
}
/* 
class Splashscreen extends StatelessWidget {
  Splashscreen() {
    Future.delayed(Duration(seconds: 2)).then((value) {
      Get.offAndToNamed(Routes.INITIAL);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[900],
      body: Container(
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
            backgroundColor: Colors.white,
          ),
        ),
      ),
    );
  }
} */

import 'package:app_pizzaria_quero_mais/custom_widgets/botao_carrinho/botao_carrinho_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';

class BotaoCarrinho extends StatelessWidget {
  BotaoCarrinhoController botaoCarrinhoController =
      Get.put(BotaoCarrinhoController());
  @override
  Widget build(BuildContext context) {
    return GetX<CarrinhoController>(builder: (_) {
      if (_.entradas.isEmpty && _.pizzas.isEmpty) return Container();
      return InkWell(
        onTap: () {
          botaoCarrinhoController.goToCarrinho();
          //Get.toNamed(Routes.CARRINHO);
        },
        child: Container(
          padding: EdgeInsets.only(left: 15, right: 15),
          height: 60,
          alignment: Alignment.center,
          width: Get.width,
          color: Colors.red,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${_.quantidade.toString()} ${_.quantidade > 1 ? 'itens' : 'item'}',
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
              Text(
                'Ir para o carrinho',
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                'R\$${_.valorTotal.toStringAsFixed(2)}',
                style: TextStyle(fontSize: 18, color: Colors.white),
              ),
            ],
          ),
        ),
      );
    });
  }
}

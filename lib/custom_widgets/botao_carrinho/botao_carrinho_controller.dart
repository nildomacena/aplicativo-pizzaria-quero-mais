import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_controller.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';
import 'package:app_pizzaria_quero_mais/services/util_service.dart';
import 'package:get/get.dart';

class BotaoCarrinhoController extends GetxController {
  AuthProvider authProvider = Get.find();
  HomeController homeController = Get.find();

  goToCarrinho() async {
    print('goo to carrinho');
    if ((await authProvider.getUsuarioByUser()) == null) {
      homeController.setTab(2);
    } else if (!(await authProvider.checkInfoUsuario())) {
      homeController.setTab(2);
      utilService.snackBar(
          titulo: 'Preencha seu dados',
          mensagem:
              'Para finalizar seu pedido, preencha seu cadastro com nome e telefone');
      homeController.setTab(2);
    } else {
      Get.toNamed(Routes.CARRINHO);
    }
  }
}

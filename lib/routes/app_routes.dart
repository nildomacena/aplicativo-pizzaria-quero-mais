abstract class Routes {
  static const INITIAL = '/';
  static const HOME = '/home';
  static const CATEGORIA = '/categoria';
  static const ITEM = '/item';
  static const CARRINHO = '/carrinho';
  static const EDITAR_DADOS = '/editar_dados';
  static const ENDERECOS = '/enderecos';
  static const NOVO_ENDERECO = '/novo_endereco';
  static const SELECIONA_LOCALIZACAO = '/seleciona_localizacao';
  static const PEDIDOS = '/pedidos';
  static const SPLASHSCREEN = '/splashscreen';
}

import 'package:app_pizzaria_quero_mais/components/splashscreen/splashscreen.dart';
import 'package:app_pizzaria_quero_mais/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_binding.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_page.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_binding.dart';
import 'package:app_pizzaria_quero_mais/components/categoria/categoria_page.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_binding.dart';
import 'package:app_pizzaria_quero_mais/components/editar_dados/editar_dados_page.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_binding.dart';
import 'package:app_pizzaria_quero_mais/components/enderecos/enderecos_page.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_binding.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_page.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_binding.dart';
import 'package:app_pizzaria_quero_mais/components/item_detail/item_detail_page.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_binding.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_controller.dart';
import 'package:app_pizzaria_quero_mais/components/novo_endereco/novo_endereco_page.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_binding.dart';
import 'package:app_pizzaria_quero_mais/components/pedidos/pedidos_page.dart';
import 'package:app_pizzaria_quero_mais/components/seleciona_localizacao/seleciona_localizacao_page.dart';
import 'package:app_pizzaria_quero_mais/routes/app_routes.dart';

class AppPages {
  static final List<GetPage> routes = [
    GetPage(
        name: Routes.INITIAL, page: () => HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.HOME, page: () => HomePage(), binding: HomeBinding()),
    GetPage(
        name: Routes.CATEGORIA,
        page: () => CategoriaPage(),
        binding: CategoriaBinding()),
    GetPage(
        name: Routes.ITEM,
        page: () => ItemDetailPage(),
        binding: ItemDetailBinding()),
    GetPage(
        name: Routes.CARRINHO,
        page: () => CarrinhoPage(),
        binding: CarrinhoBinding()),
    GetPage(
        name: Routes.EDITAR_DADOS,
        page: () => EditarDadosPage(),
        binding: EditarDadosBinding()),
    GetPage(
        name: Routes.ENDERECOS,
        page: () => EnderecosPage(),
        binding: EnderecosBinding()),
    GetPage(
        name: Routes.NOVO_ENDERECO,
        page: () => NovoEnderecoPage(),
        binding: NovoEnderecoBinding()),
    GetPage(
        name: Routes.SELECIONA_LOCALIZACAO,
        page: () => SelecionaLocalizacaoPage()),
    GetPage(
        name: Routes.PEDIDOS,
        binding: PedidosBinding(),
        page: () => PedidosPage()),
    GetPage(name: Routes.SPLASHSCREEN, page: () => Splashscreen()),
  ];
}

import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/pizza.model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PizzasProvider {
  CarrinhoController carrinhoController = Get.find();
  Rx<Pizza> _currentPizza = Pizza().obs;
  RxBool _doisSabores = false.obs;
  RxBool _escolheuPizza = false.obs;
  Stream<bool> get doisSabores$ => _doisSabores.stream;

  bool get escolheuPizza => _escolheuPizza.value;
  bool get doisSabores => _doisSabores.value;
  set doisSabores(bool value) => _doisSabores.value = value;

  Pizza get currentPizza => _currentPizza.value;
  set currentPizza(Pizza c) => _currentPizza.value = c;
  Stream<Pizza> get currentPizza$ => _currentPizza.stream;

  PizzasProvider() {
    _doisSabores.listen((value) {
      print('dois sabores atualizou: $value');
    });
  }
  Future<void> mudarQuantidadeSabores() async {
    var resultDialog;
    if (doisSabores && currentPizza.sabores.length > 1) {
      resultDialog = await Get.dialog(AlertDialog(
        title: Text(
          'ATENÇÃO!',
        ),
        content: Text(
            'Você selecionou dois sabores. Deseja mudar para UM SABOR? Ao fazer isso, o segundo sabor selecionado é descartado.'),
        actions: [
          TextButton(
              onPressed: () {
                Get.back(result: false);
              },
              child: Text('CANCELAR')),
          TextButton(
              onPressed: () {
                Get.back(result: true);
              },
              child: Text('CONFIRMAR')),
        ],
      ));
      if (resultDialog != null && resultDialog) {
        currentPizza.deleteUltimoSabor();
        doisSabores = !doisSabores;
        return;
      } else
        return;
    }
    doisSabores = !doisSabores;
  }

  void adicionarSaborPizza(Item item) {
    if (doisSabores) return _currentPizza.value.addSabor(item);
    _currentPizza.value.setUnicoSabor(item);
  }

  void deleteSabor(Item item) {
    return _currentPizza.value.deleteSabor(item);
  }

  void addPizzaAoCarrinho() {
    carrinhoController.addPizza(currentPizza);
    doisSabores = false;
    _currentPizza.value = Pizza();
  }
}

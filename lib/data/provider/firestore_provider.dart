import 'package:app_pizzaria_quero_mais/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
//import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:app_pizzaria_quero_mais/data/model/adicional.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/categoria.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/sorteio.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class FirestoreProvider {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  List<Item> cardapio = [];
  double fretePorKm = 2;
  double cashback = 0;
  FirestoreProvider() {
    getCardapio();
    getCashback();
    firebaseMessagingHandler();
  }

  firebaseMessagingHandler() {
    var initialzationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettings =
        InitializationSettings(android: initialzationSettingsAndroid);

    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                icon: android?.smallIcon,
              ),
            ));
      }
    });
  }

  Future<bool> checkModoTeste() async {
    DocumentSnapshot snapshot = await _firestore.doc('controle/controle').get();
    return snapshot['teste'] ?? false;
  }

  Future<double> getCashback() async {
    DocumentSnapshot documentSnapshot =
        await _firestore.doc('controle/controle').get();
    return cashback = documentSnapshot['cashback'] / 100 ?? 0;
  }

  Future<List<Categoria>> getCategorias() async {
    QuerySnapshot querySnapshot =
        await _firestore.collection('categorias').get();

    return querySnapshot.docs.map((s) => Categoria.fromFirestore(s)).toList();
  }

  Future<List<Item>> getDestaques() async {
    QuerySnapshot querySnapshot = await _firestore
        .collection('itensCardapio')
        .where('destaque', isEqualTo: true)
        .get();
    if (querySnapshot.docs.isEmpty) return [];
    return querySnapshot.docs.map((s) => Item.fromFirestore(s)).toList();
  }

  getCardapio() async {
    QuerySnapshot querySnapshot =
        await _firestore.collection('itensCardapio').get();
    cardapio = querySnapshot.docs.map((s) => Item.fromFirestore(s)).toList();
    fretePorKm = (await _firestore.doc('controle/controle').get())['fretePorKm']
            .toDouble() ??
        2;
  }

  Future<List<Item>> getItensPorCategoria(Categoria categoria) async {
    if (cardapio.isNotEmpty)
      return cardapio
          .where((item) => item.categoriaId == categoria.id)
          .toList();
    await getCardapio();
    return cardapio.where((item) => item.categoriaId == categoria.id).toList();
  }

  Future<List<Adicional>> getAdicionais() async {
    QuerySnapshot querySnapshot =
        await _firestore.collection('adicionais').get();
    return querySnapshot.docs.map((s) => Adicional.fromFirestore(s)).toList();
  }

  Future<dynamic> finalizarPedido(
      List<Entrada> entradas,
      Usuario usuario,
      String observacoes,
      Endereco endereco,
      FormaPagamento formaPagamento,
      double valorTotal,
      double valorTotalSemFrete,
      double cashbackUtilizado,
      int frete) async {
    DocumentReference ref = await _firestore.collection('pedidos').add({
      'entradas': entradas.map((e) => e.asMap).toList(),
      'observacoes': observacoes,
      'timestamp': FieldValue.serverTimestamp(),
      'uidUsuario': usuario.uid,
      'nomeUsuario': usuario.nome,
      'endereco': endereco.asMap,
      'usuario': usuario.asMap,
      'formaPagamento': formaPagamento.asMap,
      'valorTotal': valorTotal,
      'valorTotalSemFrete': valorTotalSemFrete,
      'cashbackUtilizado': cashbackUtilizado,
      'valorCashback':
          double.parse((valorTotalSemFrete * cashback).toStringAsFixed(2)),
      'status': 'pendente',
      'frete': frete
    }).timeout(Duration(seconds: 10));
    //return '';
    return FirebaseMessaging.instance.subscribeToTopic(ref.id);
  }

  Future<void> avaliarPedido(
      Pedido pedido, double rating, String experiencia) async {
    await _firestore
        .doc(pedido.pathFirestore)
        .update({'avaliacao': rating, 'experiencia': experiencia ?? ''});
    return;
  }

  /**SORTEIOS */

  Stream<List<Sorteio>> streamSorteiosPendentes() {
    return _firestore
        .collection('sorteios')
        .where('realizado', isEqualTo: false)
        .snapshots()
        .map((QuerySnapshot querySnapshot) =>
            querySnapshot.docs.map((s) => Sorteio.fromFirestore(s)).toList());
  }

  Stream<List<Sorteio>> streamResultadosSorteios() {
    return _firestore
        .collection('sorteios')
        .where('realizado', isEqualTo: true)
        .snapshots()
        .map((QuerySnapshot querySnapshot) =>
            querySnapshot.docs.map((s) => Sorteio.fromFirestore(s)).toList());
  }

  Future<void> participarSorteio(Usuario usuario, Sorteio sorteio) async {
    List<String> participantes = sorteio.participantes;
    if (!participantes.contains(usuario.uid))
      participantes.add(usuario.uid);
    else
      return;
    //await FirebaseMessaging.instance.subscribeToTopic(sorteio.id);
    return _firestore
        .doc('sorteios/${sorteio.id}')
        .update({'participantes': participantes});
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_mask/easy_mask.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/usuario.model.dart';
import 'package:rxdart/rxdart.dart' as rxDart;

class AuthProvider {
  final Rx<Usuario> _usuario = Usuario().obs;
  final RxList<Endereco> _enderecos = RxList<Endereco>();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'profile',
    ],
  );
  AuthProvider() {
    //_usuario.bindStream(streamUsuarioByUid());
    authStateChanges.listen((User user) async {
      print('user AuthProvider: $user');
      if (user == null) {
        _usuario.value = null;
        _enderecos.value = [];
      } else {
        Future.delayed(
            Duration(seconds: 1)); // Espera para salvar os dados no firestore
        _usuario.value = await getUsuarioByUser();
        _enderecos.value = await getEnderecosByUser();
      }
    });
    /* streamUsuarioByUid('uEYgZ1MAyZQM8xaaT7BVR6hCbzq1').listen((event) {
      print(event);
    }); */
  }
  Usuario get usuario => _usuario.value;
  set usuario(Usuario u) => _usuario.value = u;

  Stream<User> get authStateChanges => _auth.authStateChanges();
  List<Endereco> get enderecos => _enderecos.toList();
  Stream<List<Endereco>> get enderecos$ => _enderecos.stream;
  Stream<Usuario> get usuario$ => _usuario.stream;

  User get currentUser => _auth.currentUser;

  Stream<User> streamUser() {
    return _auth.authStateChanges();
  }

  Future<Usuario> getUsuarioByUser() async {
    User user = _auth.currentUser;
    if (user == null) return null;
    return Usuario.fromFirestore(
        await _firestore.doc('users/${user.uid}').get());
  }

  Future<List<Endereco>> getEnderecosByUser() async {
    User user = _auth.currentUser;
    if (user == null) return [];
    QuerySnapshot querySnapshot =
        await _firestore.collection('users/${user.uid}/enderecos').get();
    if (querySnapshot.docs.isEmpty) return [];
    return querySnapshot.docs.map((s) => Endereco.fromFirestore(s)).toList();
  }

  Future<double> getCashbackUsuario() async {
    if (currentUser == null) return 0;
    DocumentSnapshot documentSnapshot =
        await _firestore.doc('users/${currentUser.uid}').get();
    try {
      if (documentSnapshot['cashback'] != null)
        return documentSnapshot['cashback'];
    } catch (e) {
      return 0;
    }
  }

  /* Stream<dynamic> streamUsuarioByUid(String uid) {
    Stream<QuerySnapshot> snapshotEnderecos =
        _firestore.collection('usuarios/$uid/enderecos').snapshots();
    Stream<DocumentSnapshot> snapshotUsuario =
        _firestore.doc('usuarios/$uid').snapshots();
    return rxDart.CombineLatestStream([
      snapshotUsuario,
      snapshotEnderecos,
    ], (data) => Usuario.fromFirestore(data.elementAt(0), data.elementAt(1)));
    /* return _firestore
        .doc('usuarios/$uid')
        .snapshots()
        .map((s) => Usuario.fromFirestore(s, snapshotEnderecos)); */
  } */

  Stream<List<Endereco>> streamEnderecos() {
    return _auth
        .authStateChanges()
        .asyncExpand((user) {
          /* print('user: $user');
          if (user == null) return null; */
          if (user == null) return Stream.value(null);

          return _firestore
              .collection('usuarios/${user?.uid}/enderecos')
              .snapshots();
        })
        .map((querySnapshot) =>
            querySnapshot.docs.map((s) => Endereco.fromFirestore(s)).toList())
        .handleError(() {
          throw 'logout';
        });

    /*  return _auth
        .authStateChanges()
        .asyncExpand((user) => _firestore
            .collection('usuarios/${user.uid}/enderecos')
            .snapshots()
            .map((s) => Endereco.fromFirestore(s)))
        .toList(); */
  }

/*   Future<List<Endereco>> salvarEndereco(Endereco endereco) async {
    await _firestore
        .collection('usuarios/${usuario.uid}/enderecos')
        .add(endereco.asMap);
    return getEnderecosByUser();
  } */

  Stream<Usuario> streamUsuarioByUid([String uid]) {
    return _auth.authStateChanges().asyncExpand((user) {
      if (user == null) return Stream.value(null);
      return _firestore
          .doc('usuarios/${user.uid}')
          .snapshots()
          .map((s) => Usuario.fromFirestore(s));
    }).handleError(() {
      throw 'logout';
    });
    return _firestore
        .doc('usuarios/$uid')
        .snapshots()
        .map((s) => Usuario.fromFirestore(s));
  }

  Future<void> loginComGoogle() async {
    if (_usuario.value != null) {
      await _auth.signOut();
      return null;
    }
    User user;
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    AuthCredential authCredential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken);
    if (googleSignInAccount == null) return null;

    try {
      final UserCredential userCredential =
          await _auth.signInWithCredential(authCredential);
      user = userCredential.user;
      await user.updateEmail(googleSignInAccount.email);
      return checkBDUserInfo(user);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'account-exists-with-different-credential') {
        print('ocorreu um erro durante o processo: $e');
      } else if (e.code == 'invalid-credential') {
        print('ocorreu um erro durante o processo: $e');
      }
    } catch (e) {
      print('ocorreu um erro durante o processo: $e');
    }
  }

  Future<void> logout() {
    return _auth.signOut();
  }

  Future<void> checkBDUserInfo(User user) async {
    DocumentSnapshot snapshot = await _firestore.doc('users/${user.uid}').get();
    TextEditingController nomeController = TextEditingController();
    TextEditingController telefoneController = TextEditingController();
    if (snapshot.exists) {
      usuario = Usuario.fromFirestore(snapshot);
      if (usuario.nome.isNotEmpty && usuario.celular.isNotEmpty)
        return; // Verifica se as informações de nome e telefone já estão armazenadas
    }

    /* nomeController.text = user.displayName ?? '';
    await Get.dialog(
        AlertDialog(
          title: Text('Preencha seus dados para contato'),
          content: ListView(
            shrinkWrap: true,
            children: [
              TextField(
                controller: nomeController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(40),
                  filled: true,
                  fillColor: Colors.blue.shade100,
                  border: OutlineInputBorder(),
                  labelText: 'label',
                  hintText: 'Nome',
                  icon: Icon(Icons.person),
                ),
              ),
              TextField(
                controller: telefoneController,
                inputFormatters: [TextInputMask(mask: '(99)99999-9999')],
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(40),
                  filled: true,
                  fillColor: Colors.blue.shade100,
                  border: OutlineInputBorder(),
                  labelText: 'label',
                  hintText: 'Telefone',
                  icon: Icon(Icons.phone),
                ),
              ),
            ],
          ),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                },
                child: Text('SALVAR INFORMAÇÕES'))
          ],
        ),
        barrierDismissible: false); */
    await _firestore.doc('users/${user.uid}').set({
      'email': user.email,
      'nome': user.displayName,
      'celular': telefoneController.text ?? ''
    });
    usuario =
        Usuario.fromFirestore(await _firestore.doc('users/${user.uid}').get());
  }

  Future<Usuario> getCurrentUsuario() async {
    return Usuario.fromFirestore(
        await _firestore.doc('users/${_auth.currentUser.uid}').get());
  }

  Future<Usuario> salvarDadosPessoais(String nome, String celular) async {
    await _firestore
        .doc('users/${(await getCurrentUsuario()).uid}')
        .update({'nome': nome, 'celular': celular});
    return getCurrentUsuario();
  }

  Future<bool> checkInfoUsuario() async {
    //retorna se o usuário preencheu seu cadastro
    Usuario u = await getCurrentUsuario();
    print('telefone ususario ${u.celular}');
    return u.nome != null &&
        u.nome.isNotEmpty &&
        u.celular != null &&
        u.celular.isNotEmpty;
  }

  /**ENDEREÇOS */

  Future<List<Endereco>> salvarEndereco(Endereco endereco) async {
    Usuario u = await getCurrentUsuario();
    await _firestore.collection('users/${u.uid}/enderecos').add(endereco.asMap);
    return getEnderecosByUser();
  }

  Future<List<Endereco>> excluirEndereco(Endereco endereco) async {
    Usuario u = await getCurrentUsuario();
    await _firestore.doc('users/${u.uid}/enderecos/${endereco.id}').delete();
    return getEnderecosByUser();
  }

  Future<List<Endereco>> tornarEnderecoPadrao(Endereco endereco) async {
    List<Future> futures = [];
    List<Endereco> enderecos = await getEnderecosByUser();

    print('endereco: $enderecos');
    enderecos.forEach((e) {
      futures.add(_firestore
          .doc('users/${_auth.currentUser.uid}/enderecos/${e.id}')
          .update({'padrao': e.id == endereco.id}));
    });
    print('futures: $futures');
    await Future.wait(futures);
    return getEnderecosByUser();
  }

  /**PEDIDOS */
  Future<List<Pedido>> getPedidos() async {
    Usuario u = await getCurrentUsuario();

    return (await _firestore
            .collection('pedidos')
            .where('uidUsuario', isEqualTo: u.uid)
            .get())
        .docs
        .map((s) => Pedido.fromFirestore(s))
        .toList();
  }

  Stream<bool> streamPossuiPendentes() {
    return usuario$.asyncExpand((u) => usuario == null
        ? Stream.value(false)
        : _firestore
            .collection('pedidos')
            .where('uidUsuario', isEqualTo: usuario.uid)
            .snapshots()
            .map((QuerySnapshot snapshot) => snapshot.docs.isNotEmpty));
    print('Usuario streamPossuiPendentes: $usuario');
    return _firestore
        .collection('pedidos')
        .where('uidUsuario', isEqualTo: usuario.uid)
        .snapshots()
        .map((QuerySnapshot snapshot) => snapshot.docs.isNotEmpty);
  }
}

import 'dart:math';

import 'package:app_pizzaria_quero_mais/data/model/adicional.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';

class Entrada {
  final Item item;
  int codigo;
  int quantidade;
  List<Adicional> adicionais;
  String observacoes;
  Entrada(this.item, this.quantidade, {this.adicionais, this.observacoes}) {
    var rng = new Random();
    adicionais = this.adicionais ?? [];
    observacoes = this.observacoes ?? '';
    for (var i = 0; i < 10; i++) {
      codigo = rng.nextInt(100);
    }
  }

  factory Entrada.fromMap(Map map) {
    List<Adicional> adicionaisAux = [];
    if (map['adicionais'] != null && map['adicionais'].length > 0)
      map['adicionais'].forEach((adicional) {
        adicionaisAux.add(Adicional.fromMap(adicional));
      });
    /* adicionaisAux = [...map['adicionais']]; */

    return Entrada(Item.fromMap(map['item']), map['quantidade'],
        adicionais: adicionaisAux, observacoes: map['observacoes'] ?? '');
  }
  acrescentarAdicional(Adicional novoAdicional) {
    bool jaConsta = false;
    int index = 0;
    adicionais.forEach((f) {
      if (f.nome == novoAdicional.nome)
        jaConsta = true;
      else
        index++;
    });
    if (jaConsta)
      adicionais.removeAt(index);
    else
      adicionais.add(novoAdicional);
    return;
  }

  double get precoItemMaisAdicionais {
    double preco = 0;
    double precoAdicionais = 0;
    if (adicionais.length > 0)
      adicionais.forEach((adicional) {
        precoAdicionais += adicional.preco;
      });
    preco = (precoAdicionais + item.preco) * quantidade;
    print('Preço $preco');
    return preco;
  }

  double get precoTotal => precoItemMaisAdicionais;

  void adicionarItem() {
    quantidade++;
  }

  void removerItem() {
    if (quantidade > 1) quantidade--;
  }

  /**
   * 
   * final Item item;
  int codigo;
  int quantidade;
  List<Adicional> adicionais;
  String observacoes;
   */
  Map<String, dynamic> get asMap => {
        'item': item.asMap,
        'quantidade': quantidade,
        'adicionais': adicionais.map((adicional) => adicional.asMap).toList(),
        'observacoes': observacoes,
      };
  @override
  String toString() {
    return 'Item: ${item.nome} - $quantidade itens - R\$$precoTotal - Observações: $observacoes - Quantidade de adicionais: ${adicionais.length}';
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';

class Sorteio {
  final String id;
  final String imagem;
  final String titulo;
  final String descricao;
  final DateTime data;
  final bool pendente;
  final String link;
  final String instrucoes;
  final List<String> participantes;
  final String ganhador;
  final String ganhadorEmail;
  final String ganhadorUid;

  Sorteio(
      {this.id,
      this.titulo,
      this.data,
      this.descricao,
      this.imagem,
      this.pendente,
      this.instrucoes,
      this.link,
      this.participantes,
      this.ganhador,
      this.ganhadorEmail,
      this.ganhadorUid});

  factory Sorteio.fromFirestore(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    List<String> participantes = data['participantes'].cast<String>();

    print('Participantes: ${data['participantes']}');
    return Sorteio(
        id: snapshot.id,
        titulo: data['titulo'],
        descricao: data['descricao'],
        pendente: !data['realizado'],
        data: data['data'].toDate(),
        imagem: data['imagemUrl'],
        ganhador: data['ganhador'] ?? '',
        ganhadorEmail: data['ganhadorEmail'] ?? '',
        ganhadorUid: data['ganhadorUid'] ?? '',
        instrucoes: data['instrucoes'] ??
            'Marque três amigos na postagem oficial do sorteio',
        link: data['link'] ?? 'https://www.instagram.com/p/B2kJSBsBdMz/',
        participantes: participantes);
  }

  @override
  String toString() {
    return 'ID: $id - Título: $titulo - Data: $data - Pendente: $pendente - Participantes: $participantes';
  }
}

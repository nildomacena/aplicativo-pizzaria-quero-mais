import 'package:cloud_firestore/cloud_firestore.dart';

class Acompanhamento {
  final String descricao;
  final String imagemUrl;
  final String id;
  final double preco;

  Acompanhamento(
    this.id,
    this.descricao,
    this.preco,
    this.imagemUrl,
  );
  factory Acompanhamento.fromFirestore(DocumentSnapshot snapshot) {
    Map map = snapshot.data();
    return Acompanhamento(
        snapshot.id,
        map['descricao'],
        map['preco'].toDouble(),
        map['imagemUrl'] ??
            'https://firebasestorage.googleapis.com/v0/b/thiago-lanches-agios.appspot.com/o/no%20image.png?alt=media&token=686b8858-f842-42da-a7b5-df2d39ce06d6');
  }

  factory Acompanhamento.fromMap(Map map) {
    return Acompanhamento(
        '',
        map['descricao'],
        map['preco'].toDouble(),
        map['imagemUrl'] ??
            'https://firebasestorage.googleapis.com/v0/b/thiago-lanches-agios.appspot.com/o/no%20image.png?alt=media&token=686b8858-f842-42da-a7b5-df2d39ce06d6');
  }
  Map<String, dynamic> get asMap => {'descricao': descricao, 'preco': preco};

  @override
  String toString() {
    return '$descricao - R\$${preco.toStringAsFixed(2)}';
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';

class Categoria {
  final String nome;
  final String imagemUrl;
  String id;
  Categoria({this.nome, this.imagemUrl, this.id});

  factory Categoria.fromFirestore(DocumentSnapshot snapshot) {
    Map data = snapshot.data();
    return Categoria(
        id: snapshot.id, nome: data['nome'], imagemUrl: data['imagemUrl']);
  }

  @override
  String toString() {
    return '$nome - $imagemUrl - $id';
  }
}

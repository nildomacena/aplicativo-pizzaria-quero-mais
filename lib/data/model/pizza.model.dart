import 'package:app_pizzaria_quero_mais/data/model/item.model.dart';
import 'package:get/get.dart';

class Pizza {
  RxList<Item> _sabores = <Item>[].obs;
  Pizza();

  List<Item> get sabores => _sabores.toList();
  Stream<List<Item>> get sabores$ => _sabores.stream;

  double get preco {
    double aux = 0;

    if (sabores.isEmpty) return 0;
    sabores.forEach((element) {
      if (element.preco > aux) aux = element.preco;
    });
    return aux;
  }

  String get descricao {
    print('saboresss: $sabores');
    if (sabores.length == 1) return '${sabores.first.nome}';
    if (sabores.length >= 2)
      return '1/2 ${sabores.first.nome} - 1/2 ${sabores[1].nome}';
    return 'erro';
  }

  void deleteSabor(Item item) {
    return _sabores.value
        .removeWhere((element) => element.id.contains(item.id));
  }

  void deleteUltimoSabor() {
    _sabores.value.removeAt(1);
  }

  setUnicoSabor(Item item) {
    _sabores.value = [item];
  }

  void addSabor(Item item) {
    if (!item.isPizza) throw 'nao-pizza';
    if (sabores.where((element) => element.id.contains(item.id)).isNotEmpty)
      throw 'sabor-ja-escolhido';
    if (sabores.length >= 2) throw 'num-sabores-excedido';
    return _sabores.add(item);
  }
}

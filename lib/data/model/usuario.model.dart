import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/pedido.model.dart';

class Usuario {
  final String uid;
  final String nome;
  final String email;
  final String celular;
  List<Endereco> enderecos;
  List<Pedido> pedidos;
  Endereco enderecoPadrao;

  Usuario(
      {this.enderecos,
      this.nome,
      this.email,
      this.pedidos,
      this.celular,
      this.uid,
      this.enderecoPadrao});

  factory Usuario.fromFirestoreComEnderecos(
      DocumentSnapshot snapshot, QuerySnapshot enderecos) {
    Map<String, dynamic> data = snapshot.data();
    print('snapshot.data ${snapshot.data}');
    Endereco enderecoPadrao;
    if (data['enderecoPadrao'] != null) {
      enderecos.docs.forEach((snapEnd) {
        if (snapEnd.id == data['enderecoPadrao']) {
          enderecoPadrao = Endereco.fromFirestore(snapEnd);
        }
      });
    } else {
      if (enderecos.docs.length > 0)
        enderecoPadrao = Endereco.fromFirestore(enderecos.docs[0]);
    }
    return Usuario(
        uid: snapshot.id,
        nome: data['nome'] ?? '',
        email: data['email'] ?? '',
        celular: data['celular'] ?? '',
        enderecos: enderecos.docs
            .map((endereco) => Endereco.fromFirestore(endereco))
            .toList(),
        enderecoPadrao: enderecoPadrao);
  }

  factory Usuario.fromFirestore(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data();
    /* print('snapshot.data ${snapshot.data}');
    Endereco enderecoPadrao;
    if (data['enderecoPadrao'] != null) {
      enderecos.docs.forEach((snapEnd) {
        if (snapEnd.id == data['enderecoPadrao']) {
          enderecoPadrao = Endereco.fromFirestore(snapEnd);
        }
      });
    } else {
      if (enderecos.docs.length > 0)
        enderecoPadrao = Endereco.fromFirestore(enderecos.docs[0]);
    } */
    return Usuario(
      uid: snapshot.id,
      nome: data['nome'] ?? '',
      celular: data['celular'] ?? '',
      email: data['email'] ?? '',
      /* enderecos: enderecos.docs
            .map((endereco) => Endereco.fromFirestore(endereco))
            .toList(),
        enderecoPadrao: enderecoPadrao */
    );
  }

  /*  Future<dynamic> definirEnderecoPadrao(Endereco novoEnderecoPadrao) {
    enderecoPadrao = novoEnderecoPadrao;
    return fireService.definirEnderecoPadrao(uid, enderecoPadrao);
  } */

  Map<String, String> get asMap {
    return {'uid': uid, 'nome': nome, 'celular': celular};
  }

  @override
  String toString() {
    return '$nome - $celular';
  }
}

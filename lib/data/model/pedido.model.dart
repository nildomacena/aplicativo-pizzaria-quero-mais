import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:app_pizzaria_quero_mais/data/model/endereco.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/entrada.model.dart';
import 'package:app_pizzaria_quero_mais/data/model/forma_pagamento.model.dart';

class Pedido {
  final List<Entrada> entradas;
  final DateTime data;
  final Endereco endereco;
  final FormaPagamento formaPagamento;
  String status;
  String uid;
  double avaliacao;
  String id;
  String observacao;
  String nomeCliente;
  String telefoneCliente;
  Pedido(
      {this.entradas,
      this.data,
      this.avaliacao,
      this.observacao,
      this.formaPagamento,
      this.endereco,
      this.status,
      this.uid,
      this.nomeCliente,
      this.telefoneCliente,
      this.id}) {
    if (avaliacao == null) {
      avaliacao = 0;
    }
    if (observacao == null) {
      observacao = '';
    }
    if (status == null) {
      status = 'PENDENTE';
    }
  }

  String get pathFirestore => 'pedidos/$id';
  double get valorTotal {
    double soma = 0;
    entradas.forEach((e) {
      soma += e.precoTotal;
    });
    return soma;
  }

  String get statusFormatado {
    switch (status) {
      case 'rota':
        return 'Em rota';
      case 'pendente':
        return 'Pendente';
      case 'cozinha':
        return 'Na cozinha';
      case 'finalizado':
        return 'Finalizado';
      default:
        return 'Finalizado';
    }
  }

  factory Pedido.fromFirestore(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data();
    List<Entrada> entradasAux = [];
    // TODO resolver essa questão das entradas
    //    data['entradas'].map((e) => Entrada.fromMap(e)).toList();
    data['entradas'].forEach((entrada) {
      entradasAux.add(Entrada.fromMap(entrada));
      //print('Entrada.fromMap(entrada) - ${Entrada.fromMap(entrada)}');
    });
    return Pedido(
        entradas: entradasAux,
        data: data['timestamp'].toDate(),
        avaliacao: data['avaliacao'] ?? 0,
        observacao: data['observacao'] ?? '',
        formaPagamento: FormaPagamento.fromMap(data['formaPagamento']),
        endereco: Endereco.fromMap(data['endereco']),
        status: data['status'],
        id: snapshot.id,
        uid: data['usuario']['uid'],
        telefoneCliente: data['usuario']['celular'] ?? '',
        nomeCliente: data['usuario']['nome'] ?? '');
  }

  Map<String, dynamic> get asMap => {};
  String get dataHora =>
      '${data.day < 10 ? '0' + data.day.toString() : data.day}/${data.month < 10 ? '0' + data.month.toString() : data.month}/${data.year} - ${data.hour < 10 ? '0' + data.hour.toString() : data.hour}:${data.minute < 10 ? '0' + data.minute.toString() : data.minute}';

  @override
  String toString() {
    return 'toString';
  }
}

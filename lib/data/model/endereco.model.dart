import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:maps_toolkit/maps_toolkit.dart';

class Endereco {
  String id;
  final String nome;
  final String logradouro;
  final String numero;
  final String complemento;
  final String referencia;
  final String bairro;
  double lat;
  double lng;
  bool padrao;
  Endereco(
      {@required this.nome,
      @required this.logradouro,
      @required this.numero,
      this.complemento,
      @required this.referencia,
      @required this.bairro,
      @required this.lat,
      @required this.lng,
      @required this.padrao,
      @required this.id});

  factory Endereco.fromFirestore(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data();

    return Endereco(
      nome: data['nome'],
      logradouro: data['logradouro'],
      numero: data['numero'],
      complemento: data['complemento'],
      referencia: data['referencia'],
      bairro: data['bairro'],
      lat: data['lat'],
      lng: data['lng'],
      padrao: data['padrao'],
      id: snapshot.id,
    );
  }

  factory Endereco.fromMap(Map map) {
    return Endereco(
      nome: map['nome'],
      logradouro: map['logradouro'],
      numero: map['numero'],
      complemento: map['complemento'],
      referencia: map['referencia'],
      bairro: map['bairro'],
      lat: map['lat'],
      lng: map['lng'],
      padrao: map['padrao'],
      id: map['id'] ?? '',
    );
  }
  Map<String, dynamic> get asMap => {
        'nome': nome,
        'logradouro': logradouro,
        'numero': numero,
        'complemento': complemento ?? '',
        'referencia': referencia ?? '',
        'bairro': bairro,
        'lat': lat,
        'lng': lng,
        'padrao': padrao,
        'id': id
      };

  int get distance {
    double distanceBetweenPoints = SphericalUtil.computeDistanceBetween(
        LatLng(lat, lng), LatLng(-9.5795372, -35.7773366));
    print('distancia: $distanceBetweenPoints');
    return distanceBetweenPoints ~/ 1000;
  }

  @override
  String toString() {
    return '$nome - $logradouro';
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';

class Item {
  final String id;
  final String nome;
  final String descricao;
  final double preco;
  final List<dynamic> ingredientes;
  final String imagemUrl;
  final bool aceitaAdicional;
  final String categoriaId;
  final bool isPizza;

  Item(
      {this.id,
      this.nome,
      this.descricao,
      this.preco,
      this.ingredientes,
      this.imagemUrl,
      this.aceitaAdicional,
      this.isPizza,
      this.categoriaId});

  factory Item.fromFirestore(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data();
    return Item(
        id: snapshot.id,
        descricao: data['descricao'],
        nome: data['nome'],
        preco: data['preco'].toDouble() ?? 15.toDouble(),
        imagemUrl: data['imagemUrl'],
        categoriaId: data['categoriaId'],
        isPizza: data['isPizza'] ?? false,
        ingredientes: data['ingredientes'] != null
            ? data['ingredientes']
                .map((ingrediente) => ingrediente.toString())
                .toList()
            : [],
        aceitaAdicional: data['adicionais'] ?? false);
  }

  factory Item.fromMap(Map map) {
    return Item(
      id: map['id'] ?? '',
      descricao: map['descricao'],
      nome: map['nome'],
      preco: map['preco'] ?? 15,
      imagemUrl: map['imagemUrl'],
      isPizza: map['isPizza'] ?? false,
      ingredientes: map['ingredientes']
          .map((ingrediente) => ingrediente.toString())
          .toList(),
    );
  }

  String get ingredientesString {
    String strIngredientes = '';
    int aux = 1;

    ingredientes.forEach((ingrediente) {
      if (aux == 1)
        strIngredientes = ingrediente + ', ';
      else
        strIngredientes +=
            aux == ingredientes.length ? ' $ingrediente' : ' $ingrediente,';
      aux++;
    });
    return strIngredientes;
  }

  Map<String, dynamic> get asMap => {
        'nome': nome,
        'descricao': descricao,
        'preco': preco,
        'ingredientes': ingredientes,
        'imagemUrl': imagemUrl,
        'categoriaId': categoriaId,
      };

  @override
  String toString() {
    return '$nome - $categoriaId - Pizza? $isPizza';
  }
}

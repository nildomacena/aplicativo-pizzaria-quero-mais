import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormaPagamento {
  final String descricao;
  final IconData iconData;
  String observacao;
  FormaPagamento(this.descricao, this.iconData, {this.observacao});

  factory FormaPagamento.fromMap(Map map) {
    return FormaPagamento(map['descricao'], Icons.attach_money,
        observacao: map['observacao']);
  }

  String get descricaoCompleta {
    if (observacao != null && observacao.isNotEmpty)
      return '$descricao $observacao';
    return descricao;
  }

  Map<String, String> get asMap => {
        'descricao': descricao,
        'observacao': observacao,
        'iconData': iconData.toString()
      };
}

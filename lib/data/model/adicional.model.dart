import 'package:cloud_firestore/cloud_firestore.dart';

class Adicional {
  final String id;
  final String nome;
  final double preco;

  Adicional(this.id, this.nome, this.preco);

  factory Adicional.fromFirestore(DocumentSnapshot snapshot) {
    Map map = snapshot.data();
    return Adicional(snapshot.id, map['nome'], map['valor'].toDouble());
  }

  factory Adicional.fromMap(Map map) {
    return Adicional(map['nome'] ?? '', map['nome'], map['valor']);
  }
  Map<String, dynamic> get asMap => {'nome': nome, 'valor': preco};

  @override
  String toString() {
    return '$nome - $preco';
  }
}

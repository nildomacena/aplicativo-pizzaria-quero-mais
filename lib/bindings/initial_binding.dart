import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_binding.dart';
import 'package:app_pizzaria_quero_mais/components/cardapio_tab/cardapio_controller.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_binding.dart';
import 'package:app_pizzaria_quero_mais/components/home/home_controller.dart';
import 'package:app_pizzaria_quero_mais/data/provider/pizzas_provider.dart';
import 'package:get/get.dart';
import 'package:app_pizzaria_quero_mais/components/carrinho/carrinho_controller.dart';
import 'package:app_pizzaria_quero_mais/components/perfil_tab/perfil_controller.dart';
import 'package:app_pizzaria_quero_mais/components/sorteios_tab/sorteios_controller.dart';
import 'package:app_pizzaria_quero_mais/data/provider/auth_provider.dart';
import 'package:app_pizzaria_quero_mais/data/provider/firestore_provider.dart';

class InitialBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(FirestoreProvider(), permanent: true);
    Get.put(AuthProvider(), permanent: true);
    Get.put(CarrinhoController(), permanent: true);
    Get.put(PerfilController(), permanent: true);
    Get.put(SorteiosController(), permanent: true);
    Get.put(CardapioBinding(), permanent: true);
    Get.put(PizzasProvider(), permanent: true);
  }
}
